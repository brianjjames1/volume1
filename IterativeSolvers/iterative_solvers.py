# iterative_solvers.py
"""Volume 1: Iterative Solvers.
<Name> Brian James
<Class>
<Date>
"""

import numpy as np
from matplotlib import pyplot as plt
from scipy import sparse


# Helper function
def diag_dom(n, num_entries=None):
    """Generate a strictly diagonally dominant (n, n) matrix.

    Parameters:
        n (int): The dimension of the system.
        num_entries (int): The number of nonzero values.
            Defaults to n^(3/2)-n.

    Returns:
        A ((n,n) ndarray): A (n, n) strictly diagonally dominant matrix.
    """
    if num_entries is None:
        num_entries = int(n**1.5) - n
    A = np.zeros((n,n))
    rows = np.random.choice(np.arange(0,n), size=num_entries)
    cols = np.random.choice(np.arange(0,n), size=num_entries)
    data = np.random.randint(-4, 4, size=num_entries)
    for i in range(num_entries):
        A[rows[i], cols[i]] = data[i]
    for i in range(n):
        A[i,i] = np.sum(np.abs(A[i])) + 1
    return A

# Problems 1 and 2
def jacobi(A, b, tol=1e-8, maxiter=100, plot = False):
    """Calculate the solution to the system Ax = b via the Jacobi Method.

    Parameters:
        A ((n,n) ndarray): A square matrix.
        b ((n ,) ndarray): A vector of length n.
        tol (float): The convergence tolerance.
        maxiter (int): The maximum number of iterations to perform.

    Returns:
        ((n,) ndarray): The solution to system Ax = b.
    """
    n = len(A)
    xk = np.zeros(n)
    D = np.diagonal(A)
    abs_error = []
    
    for i in range(maxiter):
        xk1 = xk + (b-np.matmul(A,xk))/D
        abs_error.append(np.linalg.norm(np.matmul(A,xk1)-b,ord=np.inf))         # keep track of abs error
        if np.linalg.norm(xk1-xk,ord=np.inf) < tol:
            xk = xk1
            break
        xk = xk1
        
        
    """ If plot==True, return a plot of the absolute error """
    if plot == True:
        num_iter = len(abs_error)
        iter_points = list(range(1,num_iter+1))
        plt.semilogy(iter_points,abs_error)
        plt.title("Convergence of Jacobi Method")
        plt.xlabel("Iteration")
        plt.ylabel("Absolute Error of Approximation")
        plt.show()
    
    return xk
"""
M = np.array([[2,0,-1],[-1,3,2],[0,1,3]])
b = np.array([3,3,-1])
jacobi(M,b,plot=True)
"""

# Problem 3
def gauss_seidel(A, b, tol=1e-8, maxiter=100, plot=False):
    """Calculate the solution to the system Ax = b via the Gauss-Seidel Method.

    Parameters:
        A ((n, n) ndarray): A square matrix.
        b ((n, ) ndarray): A vector of length n.
        tol (float): The convergence tolerance.
        maxiter (int): The maximum number of iterations to perform.
        plot (bool): If true, plot the convergence rate of the algorithm.

    Returns:
        x ((n,) ndarray): The solution to system Ax = b.
    """
    
    n = len(A)
    x0 = np.zeros(n)
    absError = []
    
    for i in range(maxiter):
        previous = x0.copy()
        for j in range(n):
            x0[j] = x0[j] + 1 / A[j,j] * (b[j]- A[j].T @ x0)                # implement the gauss-seidel formula
        if plot == True:
            absError.append(np.linalg.norm(np.matmul(A,x0)-b,ord=np.inf)) 
        if np.linalg.norm(x0-previous,ord=np.inf) < tol:                    # check for convergence
            break
       
    """ If plot is true, plot the absolute Error against the number of iterations"""
    if plot == True:
        num_iter = len(absError)
        iter_points = list(range(1,num_iter+1))
        plt.semilogy(iter_points,absError)
        plt.title("Convergence of Gauss-Seidel Method")
        plt.xlabel("Iteration")
        plt.ylabel("Absolute Error of Approximation")
        plt.show()
    
    return x0

# Problem 4
def gauss_seidel_sparse(A, b, tol=1e-8, maxiter=100):
    """Calculate the solution to the sparse system Ax = b via the Gauss-Seidel
    Method.

    Parameters:
        A ((n, n) csr_matrix): A (n, n) sparse CSR matrix.
        b ((n, ) ndarray): A vector of length n.
        tol (float): The convergence tolerance.
        maxiter (int): the maximum number of iterations to perform.

    Returns:
        x ((n,) ndarray): The solution to system Ax = b.
    """
    
    n = A.shape[0]
    x = np.zeros(n)
    
    for j in range(maxiter):
        previous = x.copy()
        for i in range(n):
            rowstart = A.indptr[i]
            rowend = A.indptr[i+1]
            Aix = A.data[rowstart:rowend] @ x[A.indices[rowstart:rowend]]
            x[i] = x[i] + 1 / A[i,i] * (b[i] - Aix)                 # implement the sparse gauss-seidel formula
        if np.linalg.norm(x-previous,ord=np.inf) < tol:             # check for convergence
            break
            
    return x


# Problem 5
def sor(A, b, omega, tol=1e-8, maxiter=100):
    """Calculate the solution to the system Ax = b via Successive Over-
    Relaxation.

    Parameters:
        A ((n, n) csr_matrix): A (n, n) sparse matrix.
        b ((n, ) Numpy Array): A vector of length n.
        omega (float in [0,1]): The relaxation factor.
        tol (float): The convergence tolerance.
        maxiter (int): The maximum number of iterations to perform.

    Returns:
        ((n,) ndarray): The solution to system Ax = b.
        (bool): Whether or not Newton's method converged.
        (int): The number of iterations computed.
    """
    
    n = A.shape[0]
    x = np.zeros(n)
    converge = False
    
    for j in range(maxiter):
        previous = x.copy()
        for i in range(n):
            rowstart = A.indptr[i]
            rowend = A.indptr[i+1]
            Aix = A.data[rowstart:rowend] @ x[A.indices[rowstart:rowend]]
            x[i] = x[i] + (omega / A[i,i]) * (b[i] - Aix)               # implement the sparse gauss-seidel formula but with omega
        if np.linalg.norm(x-previous,ord=np.inf) < tol:                 # check for convergence
            converge = True
            break
            
    return x,converge,j


# Problem 6
def hot_plate(n, omega, tol=1e-8, maxiter=100, plot=False):
    """Generate the system Au = b and then solve it using sor().
    If show is True, visualize the solution with a heatmap.

    Parameters:
        n (int): Determines the size of A and b.
            A is (n^2, n^2) and b is one-dimensional with n^2 entries.
        omega (float in [0,1]): The relaxation factor.
        tol (float): The iteration tolerance.
        maxiter (int): The maximum number of iterations.
        plot (bool): Whether or not to visualize the solution.

    Returns:
        ((n^2,) ndarray): The 1-D solution vector u of the system Au = b.
        (bool): Whether or not Newton's method converged.
        (int): The number of computed iterations in SOR.
    """
    
    # Generate the matrix A
    offsets = [-n,-1,0,1,n]
    A = sparse.diags([1,1,-4,1,1],offsets, shape=(n**2,n**2))
    A = sparse.csr_matrix(A)
    # Generate array b
    rowB = np.zeros(n)
    rowB[-1] = -100
    rowB[0] = -100
    b = np.tile(rowB,n)
    
    u, converge, iterations = sor(A,b,omega,tol,maxiter)
    
    """ If plot is true, plot the heatmap """
    if plot == True:
        plt.pcolormesh(u.reshape((n,n)),cmap='coolwarm')
        plt.title("Heatmap of hot plate")
        plt.colorbar()
        plt.show()
        
    return u, converge, iterations


# Problem 7
def prob7():
    """Run hot_plate() with omega = 1, 1.05, 1.1, ..., 1.9, 1.95, tol=1e-2,
    and maxiter = 1000 with A and b generated with n=20. Plot the iterations
    computed as a function of omega.
    """
    omega = np.linspace(1,1.95,20)          # create list of omega values from 1 to 1.95
    n = 20
    computedIter = []
    for w in omega:
        computedIter.append(hot_plate(n,w,tol=1e-2,maxiter=1000)[2])
        
    plt.plot(omega,computedIter)
    plt.title("Number of iterations vs omega")
    plt.xlabel("Omega")
    plt.ylabel("Iterations")
    plt.show()
