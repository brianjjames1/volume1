01/11/22 21:26

Problem 1 (5 points):
max_path_fast('triangle.txt'):
	Solution time: 0.000284910202 seconds
	Student time:  0.000240087509 seconds (Excellent!)
max_path_fast('triangle_large.txt'):
	Solution time: 0.006560087204 seconds
	Student time:  0.005664825439 seconds (Excellent!)
Score += 5

Problem 2 (10 points):
primes_fast(100):
	Solution time: 0.000320434570 seconds
	Student time:  0.001635313034 seconds
primes_fast(10001):
	Solution time: 0.053064823151 seconds
	Student time:  0.467234134674 seconds
Score += 10

Problem 3 (5 points):
nearest_column_fast(), 10x15 A:
	Solution time: 0.000169277191 seconds
	Student time:  0.000113487244 seconds (Excellent!)
nearest_column_fast(), 1000x1005 A:
	Solution time: 0.005261421204 seconds
	Student time:  0.003623485565 seconds (Excellent!)
Score += 5

Problem 4 (5 points):
name_scores_fast():
	Solution time: 0.004973888397 seconds
	Student time:  0.003251791000 seconds (Excellent!)
Score += 5

Problem 5 (5 points):
fibonacci_digits(100) failed (475 is too low)
fibonacci_digits(1000) failed (4781 is too low)
Score += 2

Problem 6 (10 points):
ValueError: operands could not be broadcast together with shapes (46,) (199,) 

Problem 7 (5 points):
Score += 5

Code Quality (5 points):
Didn't see any comments
Score += 4

Total score: 36/50 = 72.0%


Comments:
	Good job!

-------------------------------------------------------------------------------

01/11/22 22:47

Problem 1 (5 points):
max_path_fast('triangle.txt'):
	Solution time: 0.000229120255 seconds
	Student time:  0.000191450119 seconds (Excellent!)
max_path_fast('triangle_large.txt'):
	Solution time: 0.005372285843 seconds
	Student time:  0.004642963409 seconds (Excellent!)
Score += 5

Problem 2 (10 points):
primes_fast(100):
	Solution time: 0.000256299973 seconds
	Student time:  0.001205444336 seconds
primes_fast(10001):
	Solution time: 0.093478202820 seconds
	Student time:  0.488250255585 seconds
Score += 10

Problem 3 (5 points):
nearest_column_fast(), 10x15 A:
	Solution time: 0.000191926956 seconds
	Student time:  0.000097274780 seconds (Excellent!)
nearest_column_fast(), 1000x1005 A:
	Solution time: 0.006647109985 seconds
	Student time:  0.006208181381 seconds (Excellent!)
Score += 5

Problem 4 (5 points):
name_scores_fast():
	Solution time: 0.006831884384 seconds
	Student time:  0.003344535828 seconds (Excellent!)
Score += 5

Problem 5 (5 points):
fibonacci_digits(100) failed (475 is too low)
fibonacci_digits(1000) failed (4781 is too low)
Score += 2

Problem 6 (10 points):
ValueError: operands could not be broadcast together with shapes (46,) (199,) 

Problem 7 (5 points):
Add axes labels and a title!
Score += 4

Code Quality (5 points):
Score += 5

Total score: 36/50 = 72.0%


Comments:
	Good job!

-------------------------------------------------------------------------------

