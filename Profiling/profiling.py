# profiling.py
"""Python Essentials: Profiling.
<Name> Brian James
<Class> 
<Date>
"""

# Note: for problems 1-4, you need only implement the second function listed.
# For example, you need to write max_path_fast(), but keep max_path() unchanged
# so you can do a before-and-after comparison.

import numpy as np
from numba import int64, double, jit
import time
from matplotlib import pyplot as plt


# Problem 1
def max_path(filename="triangle.txt"):
    """Find the maximum vertical path in a triangle of values."""
    with open(filename, 'r') as infile:
        data = [[int(n) for n in line.split()]
                        for line in infile.readlines()]
    N = len(data)
    def path_sum(r, c, total):
        """Recursively compute the max sum of the path starting in row r
        and column c, given the current total.
        """
        total += data[r][c]
        if r == N - 1:          # Base case.
            return total
        else:                           # Recursive case.
            return max(path_sum(r+1, c,   total),   # Next row, same column
                       path_sum(r+1, c+1, total))   # Next row, next column

    return path_sum(0, 0, 0)            # Start the recursion from the top.

def max_path_fast(filename="triangle_large.txt"):
    """Find the maximum vertical path in a triangle of values."""
    with open(filename, 'r') as infile:
        data = [[int(n) for n in line.split()]
                        for line in infile.readlines()]
    def path_sum(row):
        "Recursively compute the max sum of the path "
        n = len(data[row])

        if row == -1:   # Base case
            return data[0][0]
        else:
            for col in range(n):
                data[row][col] = data[row][col] + max(data[row+1][col],data[row+1][col+1]) # Add the current num with the max child
            return path_sum(row-1)
    
    return path_sum(len(data)-2)   # start at second last row

# Problem 2
def primes(N):
    """Compute the first N primes."""
    primes_list = []
    current = 2
    while len(primes_list) < N:
        isprime = True
        for i in range(2, current):
            if current % i == 0:
                isprime = False
        if isprime:
            primes_list.append(current)
        current += 1
    return primes_list

def primes_fast(N):
    """Compute the first N primes."""
    primes_list = []
    n = 2
    while len(primes_list) < N:
        isprime = True
        divisors = []
        sqrt_n = np.sqrt(n)
        for p in primes_list:
            if p > sqrt_n:
                index = primes_list.index(p)
                divisors = primes_list[:index]
                break
            
        for i in divisors:
            if n % i == 0:
                isprime = False
                break
        if isprime:
            primes_list.append(n)
        if n != 2:
            n += 2
        else:
            n += 1
    return primes_list

# Problem 3
def nearest_column(A, x):
    """Find the index of the column of A that is closest to x.

    Parameters:
        A ((m,n) ndarray)
        x ((m, ) ndarray)

    Returns:
        (int): The index of the column of A that is closest in norm to x.
    """
    distances = []
    for j in range(A.shape[1]):
        distances.append(np.linalg.norm(A[:,j] - x))
    return np.argmin(distances)

def nearest_column_fast(A, x):
    """Find the index of the column of A that is closest in norm to x.
    Refrain from using any loops or list comprehensions.

    Parameters:
        A ((m,n) ndarray)
        x ((m, ) ndarray)

    Returns:
        (int): The index of the column of A that is closest in norm to x.
    """
    return np.argmin(np.linalg.norm(A-x.reshape(-1,1),axis=0))


# Problem 4
def name_scores(filename="names.txt"):
    """Find the total of the name scores in the given file."""
    with open(filename, 'r') as infile:
        names = sorted(infile.read().replace('"', '').split(','))
    total = 0
    for i in range(len(names)):
        name_value = 0
        for j in range(len(names[i])):
            alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            for k in range(len(alphabet)):
                if names[i][j] == alphabet[k]:
                    letter_value = k + 1
            name_value += letter_value
        total += (names.index(names[i]) + 1) * name_value
    return total

def name_scores_fast(filename='names.txt'):
    """Find the total of the name scores in the given file."""
    with open(filename, 'r') as infile:
        names = sorted(infile.read().replace('"', '').split(','))
    total = 0
    alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    a_dict = {}
    for i, l in enumerate(alphabet):
        a_dict[l] = i+1
    for i, name in enumerate(names):
        name_value = 0
        for letter in name:
            name_value += a_dict[letter]
        total += name_value * (i+1)
        
    return total

# Problem 5
def fibonacci():
    """Yield the terms of the Fibonacci sequence with F_1 = F_2 = 1."""
    F_1 = 1
    yield F_1
    F_2 = 1
    yield F_2
    x = 0
    while True:
        x = F_1 + F_2
        yield x
        F_1 = F_2
        F_2 = x

def fibonacci_digits(N=1000):
    """Return the index of the first term in the Fibonacci sequence with
    N digits.

    Returns:
        (int): The index.
    """
    for i, n in enumerate(fibonacci()):
        if len(str(n)) >= N:
            return i

# Problem 6fib
def prime_sieve(N):
    """Yield all primes that are less than N."""
    int_list = list(range(2,N+1))   # list of all integers from 2 to N
    for i in int_list:
        p = i
        int_list = [k for k in int_list if k % i != 0]
        yield p

# Problem 7
def matrix_power(A, n):
    """Compute A^n, the n-th power of the matrix A."""
    product = A.copy()
    temporary_array = np.empty_like(A[0])
    m = A.shape[0]
    for power in range(1, n):
        for i in range(m):
            for j in range(m):
                total = 0
                for k in range(m):
                    total += product[i,k] * A[k,j]
                temporary_array[j] = total
            product[i] = temporary_array
    return product

@jit(nopython=True, locals=dict(A=double[:,:], m=int64, n=int64,product=double[:,:], temporary_array=double[:],
                                total=double))
def matrix_power_numba(A, n):
    """Compute A^n, the n-th power of the matrix A, with Numba optimization."""
    product = A.copy()
    temporary_array = np.empty_like(A[0])
    m = A.shape[0]
    for power in range(1, n):
        for i in range(m):
            for j in range(m):
                total = 0
                for k in range(m):
                    total += product[i,k] * A[k,j]
                temporary_array[j] = total
            product[i] = temporary_array
    return product

def prob7(n=10):
    """Time matrix_power(), matrix_power_numba(), and np.linalg.matrix_power()
    on square matrices of increasing size. Plot the times versus the size.
    """
    mp_times = np.array([])
    mp_numba_times = np.array([])
    np_mp_times = np.array([])
    m_sizes = [2**i for i in range(2,8)]
    
    matrix_power_numba(np.array(np.random.random((2,2))),1)     # Run once so matrix_power_numba compiles
    
    for m in m_sizes:
        A = np.random.random((m,m))
        
        start = time.time()
        matrix_power(A,n)
        end = time.time()-start
        mp_times = np.append(mp_times,end)
        
        start = time.time()
        matrix_power_numba(A,n)
        end = time.time()-start
        mp_numba_times = np.append(mp_numba_times,end)
        
        start = time.time()
        np.linalg.matrix_power(A,n)
        end = time.time()-start
        np_mp_times = np.append(np_mp_times,end)
        
    plt.loglog(m_sizes,mp_times,color="blue",label="matrix_power")
    plt.loglog(m_sizes,mp_numba_times,color="red",label="matrix_power_numba")
    plt.loglog(m_sizes,np_mp_times,color="green",label="numpy")
    plt.legend()
    plt.show()
