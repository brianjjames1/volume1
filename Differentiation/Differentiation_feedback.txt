01/22/22 09:39

Problem 1 (5 points):
Score += 5

Problem 2 (5 points):
Score += 5

Problem 3 (10 points):
Add axes labels please!
Score += 8

Problem 4 (10 points):
prob4() incorrect
Score += 0

Problem 5 (5 points):
NotImplementedError: Problem 5 Incomplete

Problem 6 (5 points):
NotImplementedError: Problem 6 Incomplete

Problem 7 (5 points):
NotImplementedError: Problem 7 Incomplete

Code Quality (5 points):
Score += 5

Total score: 23/50 = 46.0%


Comments:
	Keep it up!

-------------------------------------------------------------------------------

01/26/22 20:05

Problem 1 (5 points):
Score += 5

Problem 2 (5 points):
Score += 5

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
Score += 10

Problem 5 (5 points):
Score += 5

Problem 6 (5 points):
Score += 5

Problem 7 (5 points):
Score += 5

Code Quality (5 points):
Score += 5

Total score: 50/50 = 100.0%

Excellent!


Comments:
	Awesome!

-------------------------------------------------------------------------------

