# differentiation.py
"""Volume 1: Differentiation.
<Name> Brian James
<Class>
<Date>
"""
import sympy as sy
from matplotlib import pyplot as plt
import numpy as np
import random
import time

from autograd import numpy as anp
from autograd import grad
from autograd import elementwise_grad
from sympy import factorial

# Problem 1
def prob1():
    """Return the derivative of (sin(x) + 1)^sin(cos(x)) using SymPy."""
    x = sy.symbols('x')
    exp = (sy.sin(x)+1)**(sy.sin(sy.cos(x)))
    diff = sy.diff(exp,x)
    f = sy.lambdify(x,diff,'numpy')
    return f

# Problem 2
def fdq1(f, x, h=1e-5):
    """Calculate the first order forward difference quotient of f at x.
    param: 
        f: function handle
        x: array of points
        h: small non-zero number to approximate derivative with
    """
    
    f_xh = f(x+h)
    f_x = f(x)
    return (f_xh-f_x)/h

def fdq2(f, x, h=1e-5):
    """Calculate the second order forward difference quotient of f at x."""
    f_xh = f(x+h)
    f_x = f(x)
    f_x2h = f(x+2*h)
    return (-3*f_x+4*f_xh-f_x2h) / (2*h)

def bdq1(f, x, h=1e-5):
    """Calculate the first order backward difference quotient of f at x."""
    f_xh = f(x-h)
    f_x = f(x)
    return (f_x-f_xh)/h

def bdq2(f, x, h=1e-5):
    """Calculate the second order backward difference quotient of f at x."""
    f_xh = f(x-h)
    f_x = f(x)
    f_x2h = f(x-2*h)
    return (3*f_x-4*f_xh+f_x2h)/(2*h)

def cdq2(f, x, h=1e-5):
    """Calculate the second order centered difference quotient of f at x."""
    pos_f_xh = f(x+h)
    neg_f_xh = f(x-h)
    return (pos_f_xh - neg_f_xh) / (2*h)

def cdq4(f, x, h=1e-5):
    """Calculate the fourth order centered difference quotient of f at x."""
    neg_f_xh = f(x-h)
    neg_f_x2h = f(x-2*h)
    pos_f_xh = f(x+h)
    pos_f_x2h = f(x+2*h)
    return (neg_f_x2h - 8*neg_f_xh+8*pos_f_xh - pos_f_x2h) / (12*h)
    

# Problem 3
def prob3(x0):
    """Let f(x) = (sin(x) + 1)^(sin(cos(x))). Use prob1() to calculate the
    exact value of f'(x0). Then use fdq1(), fdq2(), bdq1(), bdq2(), cdq1(),
    and cdq2() to approximate f'(x0) for h=10^-8, 10^-7, ..., 10^-1, 1.
    Track the absolute error for each trial, then plot the absolute error
    against h on a log-log scale.

    Parameters:
        x0 (float): The point where the derivative is being approximated.
    """
    
    x = sy.symbols('x')
    
    h_range = np.logspace(-8,0,9)     # Get 9 values from 1e-8 to 1e0
    
    f = sy.lambdify(x,(sy.sin(x)+1)**(sy.sin(sy.cos(x))),'numpy')
    f_prime_exact = prob1()(x0)
    
    # list for all methods' errors
    fdq1_error = []
    fdq2_error = []
    bdq1_error = []
    bdq2_error = []
    cdq2_error = []
    cdq4_error = []
    
    for h in h_range:
    
        #fdq1
        a_fdq1 = fdq1(f,x0,h)
        fdq1_error.append(np.abs(f_prime_exact - a_fdq1))
        
        #fdq2
        a_fdq2 = fdq2(f,x0,h)
        fdq2_error.append(np.abs(f_prime_exact - a_fdq2))
        
        #bdq1
        a_bdq1 = bdq1(f,x0,h)
        bdq1_error.append(np.abs(f_prime_exact - a_bdq1))
        
        #bdq2
        a_bdq2 = bdq2(f,x0,h)
        bdq2_error.append(np.abs(f_prime_exact - a_bdq2))
        
        #cdq2
        a_cdq2 = cdq2(f,x0,h)
        cdq2_error.append(np.abs(f_prime_exact - a_cdq2))     
        
        #cdq4
        a_cdq4 = cdq4(f,x0,h)
        cdq4_error.append(np.abs(f_prime_exact - a_cdq4))
        
    # plot all the errors
    plt.loglog(h_range,fdq1_error,color='blue',label='Order 1 Forward')
    plt.loglog(h_range,fdq2_error,color='orange',label='Order 2 Forward')
    plt.loglog(h_range,bdq1_error,color='green',label='Order 1 Backward')
    plt.loglog(h_range,bdq2_error,color='red',label='Order 2 Backward')
    plt.loglog(h_range,cdq2_error,color='purple',label='Order 2 Centered')
    plt.loglog(h_range,cdq4_error,color='brown',label='Order 4 Centered')
    plt.legend()
    plt.ylabel("Absolute Error")
    plt.xlabel("h")
    plt.show()
    

# Problem 4
def prob4():
    """The radar stations A and B, separated by the distance 500m, track a
    plane C by recording the angles alpha and beta at one-second intervals.
    Your goal, back at air traffic control, is to determine the speed of the
    plane.

    Successive readings for alpha and beta at integer times t=7,8,...,14
    are stored in the file plane.npy. Each row in the array represents a
    different reading; the columns are the observation time t, the angle
    alpha (in degrees), and the angle beta (also in degrees), in that order.
    The Cartesian coordinates of the plane can be calculated from the angles
    alpha and beta as follows.

    x(alpha, beta) = a tan(beta) / (tan(beta) - tan(alpha))
    y(alpha, beta) = (a tan(beta) tan(alpha)) / (tan(beta) - tan(alpha))

    Load the data, convert alpha and beta to radians, then compute the
    coordinates x(t) and y(t) at each given t. Approximate x'(t) and y'(t)
    using a first order forward difference quotient for t=7, a first order
    backward difference quotient for t=14, and a second order centered
    difference quotient for t=8,9,...,13. Return the values of the speed at
    each t.
    """
    data = np.load("plane.npy")
    times = data[:,0]
    alpha = np.deg2rad(data[:,1])
    beta = np.deg2rad(data[:,2])
    a = 500
    
    x_coor = a*np.tan(beta)/(np.tan(beta)-np.tan(alpha))
    y_coor = a*np.tan(beta)*np.tan(alpha)/(np.tan(beta)-np.tan(alpha))
    
    speed_list = []
    
    def speed(x,y):
        return np.sqrt(x**2+y**2)
    
    for i in range(len(times)):
        t = i + 7
        if t == 7:      # first order forward difference quotient
            x_prime = x_coor[i+1]-x_coor[i]
            y_prime = y_coor[i+1]-y_coor[i]
            
        elif t == 14:       #first order backward difference
            x_prime = x_coor[i]-x_coor[i-1]
            y_prime = y_coor[i]-y_coor[i-1]
            
        else:       # second order centered difference
            x_prime = (x_coor[i+1]-x_coor[i-1])/2
            y_prime = (y_coor[i+1]-y_coor[i-1])/2
            
        speed_list.append(speed(x_prime,y_prime))
    
    return np.array(speed_list)

# Problem 5
def jacobian_cdq2(f, x, h=1e-5):
    """Approximate the Jacobian matrix of f:R^n->R^m at x using the second
    order centered difference quotient.

    Parameters:
        f (function): the multidimensional function to differentiate.
            Accepts a NumPy (n,) ndarray and returns an (m,) ndarray.
            For example, f(x,y) = [x+y, xy**2] could be implemented as follows.
            >>> f = lambda x: np.array([x[0] + x[1], x[0] * x[1]**2])
        x ((n,) ndarray): the point in R^n at which to compute the Jacobian.
        h (float): the step size in the finite difference quotient.

    Returns:
        ((m,n) ndarray) the Jacobian matrix of f at x.
    """
    
    m = len(f(x))
    n = len(x)
    I = np.eye(n)
    J = []

    for j in range(n):
        J.append((f(x +h*I[:,j]) - f(x-h*I[:,j]))/(2*h))
    J = np.transpose(J)
    return J
    
# Problem 6
def cheb_poly(x, n):
    """Compute the nth Chebyshev polynomial at x.

    Parameters:
        x (autograd.ndarray): the points to evaluate T_n(x) at.
        n (int): The degree of the polynomial.
    """
    # End cases
    if n == 0:
        return anp.ones_like(x)
    elif n == 1:
        return x
    else:
        return 2*x*cheb_poly(x,n-1) - cheb_poly(x,n-2)

def prob6():
    """Use Autograd and cheb_poly() to create a function for the derivative
    of the Chebyshev polynomials, and use that function to plot the derivatives
    over the domain [-1,1] for n=0,1,2,3,4.
    """
    pts = anp.linspace(-1,1,100)
    d_cheb = elementwise_grad(cheb_poly)
    for n in range(5):
        output = d_cheb(pts,n)
        plt.plot(pts,output,label='T\'_'+str(n)+'(x)')
        
    plt.legend()
    plt.xlabel("x")
    plt.ylabel('y')
    plt.title("Derivative of Chebyshev Polynomials")
    plt.show()

# Problem 7
def prob7(N=200):
    """Let f(x) = (sin(x) + 1)^sin(cos(x)). Perform the following experiment N
    times:

        1. Choose a random value x0.
        2. Use prob1() to calculate the “exact” value of f′(x0). Time how long
            the entire process takes, including calling prob1() (each
            iteration).
        3. Time how long it takes to get an approximation of f'(x0) using
            cdq4(). Record the absolute error of the approximation.
        4. Time how long it takes to get an approximation of f'(x0) using
            Autograd (calling grad() every time). Record the absolute error of
            the approximation.

    Plot the computation times versus the absolute errors on a log-log plot
    with different colors for SymPy, the difference quotient, and Autograd.
    For SymPy, assume an absolute error of 1e-18.
    """
    
    f = lambda x : (anp.sin(x) + 1)**(anp.sin(anp.cos(x)))  # define function
    
    prob1Times = []
    cdq4Times= []
    cdq4Error = []
    autogradTimes = []
    autogradError = []
    
    for n in range(N):
        x_0 = random.uniform(0,100)     # generate a random number between 0-100
        
        # Sympy times
        start = time.time()
        exact = prob1()(x_0)
        prob1Times.append(time.time()-start)    # record the amount of time the process took
        
        # cdq4 times
        start = time.time()
        approx = cdq4(f,x_0)
        cdq4Times.append(time.time()-start)     # record the time taken
        cdq4Error.append(np.abs(exact-approx))  # record the absolute error
        
        # Autograd times
        start = time.time()
        approx = grad(f)(x_0)
        autogradTimes.append(time.time()-start)
        autogradError.append(np.abs(exact-approx))
        
    # Plot each of the methods
    
    plt.loglog(prob1Times,[1e-18]*N,'bo', alpha=.4,label='SymPy')
    plt.loglog(cdq4Times,cdq4Error,'o',alpha=.4,label='Difference Quotients',color='orange')
    plt.loglog(autogradTimes,autogradError,'go',alpha=.4,label='Autograd')
    plt.xlabel("Computation Time (seconds)")
    plt.ylabel("Absolute Error")
    plt.legend()
    plt.show()