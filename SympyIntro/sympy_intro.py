# sympy_intro.py
"""Python Essentials: Introduction to SymPy.
<Name> Brian James
<Class>
<Date>
"""

import sympy as sy
import numpy as np
from matplotlib import pyplot as plt

# Problem 1
def prob1():
    """Return an expression for

        (2/5)e^(x^2 - y)cosh(x+y) + (3/7)log(xy + 1).

    Make sure that the fractions remain symbolic.
    """
    
    x, y = sy.symbols('x, y')
    return sy.Rational(2,5)*sy.E**(x**2-y)*sy.cosh(x+y) + sy.Rational(3,7)*sy.log(x*y+1)

# Problem 2
def prob2():
    """Compute and simplify the following expression.

        product_(i=1 to 5)[ sum_(j=i to 5)[j(sin(x) + cos(x))] ]
    """
    x, i, j = sy.symbols('x, i, j')
    exp = sy.product(sy.summation(j*(sy.sin(x)+sy.cos(x)),(j,i,5)),(i,1,5))
    return sy.simplify(exp)

# Problem 3
def prob3(N):
    """Define an expression for the Maclaurin series of e^x up to order N.
    Substitute in -y^2 for x to get a truncated Maclaurin series of e^(-y^2).
    Lambdify the resulting expression and plot the series on the domain
    y in [-2,2]. Plot e^(-y^2) over the same domain for comparison.
    """
    x, y, n = sy.symbols('x, y, n')
    exp = sy.summation(x**n/sy.factorial(n),(n,0,N))
    new_exp = exp.subs({x:-(y**2)})
    f = sy.lambdify(y,new_exp,'numpy')
    y_range = np.linspace(-2,2,601)
    output = f(y_range) # Evaluate f() at many y points [-2,2]
    
    plt.plot(y_range,output,color="blue",label='Maclaurin series')
    plt.plot(y_range,np.exp(-(y_range**2)),color="red",label="e^(-y^2)")
    plt.legend()
    plt.title("Problem 3")
    plt.show()
    

# Problem 4
def prob4():
    """The following equation represents a rose curve in cartesian coordinates.

    0 = 1 - [(x^2 + y^2)^(7/2) + 18x^5 y - 60x^3 y^3 + 18x y^5] / (x^2 + y^2)^3

    Construct an expression for the nonzero side of the equation and convert
    it to polar coordinates. Simplify the result, then solve it for r.
    Lambdify a solution and use it to plot x against y for theta in [0, 2pi].
    """
    x, y, theta, r = sy.symbols('x, y, theta, r')
    exp = 1 - ((x**2 + y**2)**(sy.Rational(7,2))+ 18*x**5*y-60*x**3*y**3+18*x*y**5)/(x**2+y**2)**3
    new_exp = exp.subs({x:r*sy.cos(theta),y:r*sy.sin(theta)})
    new_exp = sy.simplify(new_exp)
    sol = sy.solve(new_exp, r)[0]   # pick the first solution from the (2, ) array
    r = sy.lambdify(theta,sol,"numpy")
    theta_space = np.linspace(0,2*np.pi,300)
    r_theta = r(theta_space)
    
    cos = sy.lambdify(theta,sy.cos(theta),"numpy")
    sin = sy.lambdify(theta,sy.sin(theta),"numpy")
    cos_theta = cos(theta_space)
    sin_theta = sin(theta_space)
    
    x_theta = np.multiply(r_theta,cos_theta)
    y_theta = np.multiply(r_theta,sin_theta)
    
    plt.plot(x_theta,y_theta)
    plt.title("Problem 4")
    plt.show()

# Problem 5
def prob5():
    """Calculate the eigenvalues and eigenvectors of the following matrix.

            [x-y,   x,   0]
        A = [  x, x-y,   x]
            [  0,   x, x-y]

    Returns:
        (dict): a dictionary mapping eigenvalues (as expressions) to the
            corresponding eigenvectors (as SymPy matrices).
    """
    x,y,z,l = sy.symbols('x,y,z,l')
    A = sy.Matrix([[x-y,x,0],[x,x-y,x],[0,x,x-y]])
    l_I = l * sy.eye(3)
    M = A - l_I
    det_M = M.det()
    eigenvalues = sy.solve(det_M,l)
    
    e_dict = {}
    
    for lamb in eigenvalues:
        sub_M = M.subs({l:lamb})
        e_dict[lamb] = sub_M.nullspace()
        
    return e_dict


# Problem 6
def prob6():
    """Consider the following polynomial.

        p(x) = 2*x^6 - 51*x^4 + 48*x^3 + 312*x^2 - 576*x - 100

    Plot the polynomial and its critical points. Determine which points are
    maxima and which are minima.

    Returns:
        (set): the local minima.
        (set): the local maxima.
    """
    
    x = sy.symbols('x')
    p = 2*x**6 - 51*x**4 + 48*x**3 + 312*x**2 -576*x - 100
    d1_p = sy.Derivative(p,x)
    d1_p = d1_p.doit()
    d2_p = sy.Derivative(d1_p,x)
    d2_p = d2_p.doit()
    crit_p = sy.solve(d1_p,x)
    minima = []     # List of all minima
    maxima = []     # List of all maxima
    
    # find critical points
    for point in crit_p:
        output = d2_p.subs({x:point})
        if output < 0:  # point is a maxima
            maxima.append(point)
        elif output > 0:    # point is a minima
            minima.append(point)
    domain = np.linspace(-5,5,80)
    f = sy.lambdify(x,p,'numpy')
    codomain = f(domain)
    minima = np.array(minima)
    maxima = np.array(maxima)
    min_out = f(minima)
    max_out = f(maxima)
    
    # Plot the function from [-5,5]
    
    plt.plot(domain,codomain,label="p(x) = 2*x^6 - 51*x^4 + 48*x^3 + 312*x^2 - 576*x - 100")
    plt.scatter(minima,min_out,label='minima')
    plt.scatter(maxima,max_out,label='maxima')
    plt.legend()
    plt.title("Problem 6")
    plt.xlim([-6,6])
    plt.show()

# Problem 7
def prob7():
    """Calculate the integral of f(x,y,z) = (x^2 + y^2 + z^2)^2 over the
    sphere of radius r. Lambdify the resulting expression and plot the integral
    value for r in [0,3]. Return the value of the integral when r = 2.

    Returns:
        (float): the integral of f over the sphere of radius 2.
    """
    
    p,theta,phi,r = sy.symbols('p, theta, phi, r')
    
    #components of h
    h1 = p*sy.sin(phi)*sy.cos(theta)
    h2 = p*sy.sin(phi)*sy.sin(theta)
    h3 = p*sy.cos(phi)
    #h
    h = sy.Matrix([h1,h2,h3])
    
    #Jacobian of function h
    J = h.jacobian([p,theta,phi])
    det_J = J.det()
    det_J = sy.simplify(det_J)
    
    #function in terms of h
    f = (h1**2 + h2**2 + h3**2)**2
    
    #Simplify |det(J)|
    abs_det_J = -1*det_J
    
    exp = sy.simplify(f*abs_det_J)
    I1 = sy.integrate(exp,(p,0,r))

    I2 = sy.integrate(I1,(theta,0,2*sy.pi))

    I3 = sy.integrate(I2,(phi,0,sy.pi))

    L = sy.lambdify(r,I3,'numpy') #integral function with respect to r
    domain = np.linspace(0,3,80)
    
    output = L(domain)  # result of integral from r = 0 to 3
    plt.plot(domain,output,label='Integral of function in terms of r')
    plt.title("Problem 7")
    plt.legend()
    plt.show()
    
    return L(2)