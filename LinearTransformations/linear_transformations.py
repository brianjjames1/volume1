# linear_transformations.py
"""Volume 1: Linear Transformations.
<Name> Brian James
<Class> Sec 03
<Date> 9/21/2021
"""

from random import random
import numpy as np
from matplotlib import pyplot as plt
import time

# Problem 1
def stretch(A, a, b):
    """Scale the points in A by a in the x direction and b in the
    y direction.

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        a (float): scaling factor in the x direction.
        b (float): scaling factor in the y direction.
    """
    A[0,:] = [i*a for i in A[0,:]]
    A[1,:] = [i*b for i in A[1,:]]
    return A

def shear(A, a, b):
    """Slant the points in A by a in the x direction and b in the
    y direction.

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        a (float): scaling factor in the x direction.
        b (float): scaling factor in the y direction.
    """
    A[0,:] = [i+a*A[1,ind] for ind, i in enumerate(A[0,:])]
    A[1,:] = [A[0,ind]*b + i for ind, i in enumerate(A[1,:])]
    return A

def reflect(A, a, b):
    """Reflect the points in A about the line that passes through the origin
    and the point (a,b).

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        a (float): x-coordinate of a point on the reflecting line.
        b (float): y-coordinate of the same point on the reflecting line.
    """
    divisor = a**2+b**2
    a_minus_b_sqr = a**2 - b**2
    b_minus_a_sqr = b**2 - a**2
    coeff = 2*a*b
    A[0,:] = [(x*a_minus_b_sqr+A[1,ind]*coeff)/divisor for ind, x in enumerate(A[0,:])]
    A[1,:] = [(A[0,ind]*coeff+y*b_minus_a_sqr)/divisor for ind, y in enumerate(A[1,:])]
    return A

def rotate(A, theta):
    """Rotate the points in A about the origin by theta radians.

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        theta (float): The rotation angle in radians.
    """
    cosine= np.cos(theta)
    sine = np.sin(theta)
    
    row=len(A)
    col=len(A[0])
    B = np.zeros((row,col))
    """
    B[0] = [x*cosine-A[1][ind]*sine for ind, x in enumerate(A[0:])]
    B[1] = [A[0][ind]*sine + y*cosine for ind, y in enumerate(A[1:])]
    A = B
    """
    
    for i in range(len(A[0])):
        B[0][i] = A[0][i]*cosine-A[1][i]*sine
        B[1][i] = A[0][i]*sine + A[1][i]*cosine
        
    A = B
    
    return A


# Problem 2
def solar_system(T, x_e, x_m, omega_e, omega_m):
    """Plot the trajectories of the earth and moon over the time interval [0,T]
    assuming the initial position of the earth is (x_e,0) and the initial
    position of the moon is (x_m,0).

    Parameters:
        T (int): The final time.
        x_e (float): The earth's initial x coordinate.
        x_m (float): The moon's initial x coordinate.
        omega_e (float): The earth's angular velocity.
        omega_m (float): The moon's angular velocity.
    """
    """
    T = 3*np.pi/2
    x_e = 10
    x_m = 11
    omega_e = 1
    omega_m = 13
    """
    earth_initial = [[x_e],[0]]
    moon_initial = [[x_m-x_e],[0]]
    
    time_interval = np.linspace(0,T,1000)
    
    earth_pos = [rotate(earth_initial, omega_e * time) for time in time_interval]
    earth_pos = np.concatenate(earth_pos,axis=1)
    moon_pos = [rotate(moon_initial, omega_m * time) for time in time_interval]
    moon_pos = np.concatenate(moon_pos,axis=1) + earth_pos
    
    plt.gca().set_aspect("equal")
    plt.plot(earth_pos[0],earth_pos[1])
    plt.plot(moon_pos[0],moon_pos[1])
    plt.show()    


def random_vector(n):
    """Generate a random vector of length n as a list."""
    return [random() for i in range(n)]

def random_matrix(n):
    """Generate a random nxn matrix as a list of lists."""
    return [[random() for j in range(n)] for i in range(n)]

def matrix_vector_product(A, x):
    """Compute the matrix-vector product Ax as a list."""
    m, n = len(A), len(x)
    return [sum([A[i][k] * x[k] for k in range(n)]) for i in range(m)]

def matrix_matrix_product(A, B):
    """Compute the matrix-matrix product AB as a list of lists."""
    m, n, p = len(A), len(B), len(B[0])
    return [[sum([A[i][k] * B[k][j] for k in range(n)])
                                    for j in range(p) ]
                                    for i in range(m) ]

# Problem 3
def prob3():
    """Use time.time(), timeit.timeit(), or %timeit to time
    matrix_vector_product() and matrix-matrix-mult() with increasingly large
    inputs. Generate the inputs A, x, and B with random_matrix() and
    random_vector() (so each input will be nxn or nx1).
    Only time the multiplication functions, not the generating functions.

    Report your findings in a single figure with two subplots: one with matrix-
    vector times, and one with matrix-matrix times. Choose a domain for n so
    that your figure accurately describes the growth, but avoid values of n
    that lead to execution times of more than 1 minute.
    """
    
    """
    Generate random data
    """
    n_values = [1,10,25,50,100,135,250]
    vector_list = [random_vector(n) for n in n_values]
    matrix_list_A = [random_matrix(n) for n in n_values]
    matrix_list_B = [random_matrix(n) for n in n_values]
    mvp_times = []
    mmp_times = []
    
    """ 
    Time data
    """
    
    for i in range(len(n_values)):
        start = time.time()
        matrix_vector_product(matrix_list_A[i],vector_list[i])
        mvp_times.append(time.time()-start)
    
    for j in range(len(n_values)):
        start = time.time()
        matrix_matrix_product(matrix_list_A[j],matrix_list_B[j])
        mmp_times.append(time.time()-start)
        
        
    """ Plot data """
        
    ax1 = plt.subplot(121)
    ax1.plot(n_values,mvp_times,'g.-',linewidth=2,markersize=15)
    plt.xlabel("n",fontsize=14)
    plt.ylabel("Seconds",fontsize=14)
    plt.title("Matrix-Vector",fontsize=18)
    
    ax2 = plt.subplot(122)
    ax2.plot(n_values,mmp_times,'b.-',linewidth=2,markersize=15)
    plt.xlabel("n",fontsize=14)
    plt.ylabel("Seconds",fontsize=14)
    plt.title("Matrix-Matrix",fontsize=18)
    
    plt.show()
    


# Problem 4
def prob4():
    """Time matrix_vector_product(), matrix_matrix_product(), and np.dot().

    Report your findings in a single figure with two subplots: one with all
    four sets of execution times on a regular linear scale, and one with all
    four sets of exections times on a log-log scale.
    """
    """
    Generate data
    """
    n_values = [1,10,25,50,100,135,250]
    vector_list = [random_vector(n) for n in n_values]
    matrix_list_A = [random_matrix(n) for n in n_values]
    matrix_list_B = [random_matrix(n) for n in n_values]
    mvp_times = []
    mmp_times = []
    mvdot_times = []
    mmdot_times = []
    
    """
    Time data
    """
    
    for i in range(len(n_values)):
        start = time.time()
        matrix_vector_product(matrix_list_A[i],vector_list[i])
        mvp_times.append(time.time()-start)
    
    for j in range(len(n_values)):
        start = time.time()
        matrix_matrix_product(matrix_list_A[j],matrix_list_B[j])
        mmp_times.append(time.time()-start)
        
    for k in range(len(n_values)):
        start = time.time()
        np.dot(matrix_list_A[k],vector_list[k])
        mvdot_times.append(time.time()-start)
        
    for l in range(len(n_values)):
        start = time.time()
        np.dot(matrix_list_A[l],matrix_list_B[l])
        mmdot_times.append(time.time()-start)
        
    """Plot data in both linear and loglog scales """
    
    ax1 = plt.subplot(121)
    ax1.plot(n_values,mvp_times,'g.-',linewidth=.5,markersize=15,label="Matrix-Vector-Product")
    ax1.plot(n_values,mmp_times,'r.-',linewidth=.5,markersize=15,label="Matrix-Matrix-Product")
    ax1.plot(n_values,mvdot_times,'b.-',linewidth=.5,markersize=15,label="Matrix-Vector-Dot")
    ax1.plot(n_values,mmdot_times,'k.-',linewidth=.5,markersize=15,label="Matrix-Matrix-Dot")
    ax1.legend(loc="upper left")
    plt.xlabel("n",fontsize=14)
    plt.ylabel("Seconds",fontsize=14)
    plt.title("Linear",fontsize=18)
    
    ax2 = plt.subplot(122)
    ax2.loglog(n_values,mvp_times,'g.-',basex=2,basey=2,linewidth=2,markersize=15,label="Matrix-Vector-Product")
    ax2.loglog(n_values,mmp_times,'r.-',basex=2,basey=2,linewidth=2,markersize=15,label="Matrix-Matrix-Product")
    ax2.loglog(n_values,mvdot_times,'b.-',basex=2, basey=2,linewidth=2,markersize=15,label="Matrix-Vector-Dot")
    ax2.loglog(n_values,mmdot_times,'k.-',basex=2, basey=2,linewidth=2,markersize=15,label="Matrix-Matrix-Dot")
    ax2.legend(loc="upper left")
    plt.xlabel("n",fontsize=14)
    plt.ylabel("Seconds",fontsize=14)
    plt.title("LogLog",fontsize=18)
    
    plt.show()