# solutions.py
"""Volume 1: The Page Rank Algorithm.
<Name> Brian James
<Class>
<Date>
"""

import numpy as np
from collections import OrderedDict
import csv
import networkx as nx
import itertools


# Problems 1-2
class DiGraph:
    """A class for representing directed graphs via their adjacency matrices.

    Attributes:
        (fill this out after completing DiGraph.__init__().)
    """
    # Problem 1
    def __init__(self, A, labels=None):
        """Modify A so that there are no sinks in the corresponding graph,
        then calculate Ahat. Save Ahat and the labels as attributes.

        Parameters:
            A ((n,n) ndarray): the adjacency matrix of a directed graph.
                A[i,j] is the weight of the edge from node j to node i.
            labels (list(str)): labels for the n nodes in the graph.
                If None, defaults to [0, 1, ..., n-1].
        """
        n = len(A)
        self.Ahat = np.copy(A).astype(float)
        if labels==None:
            self.labels = [i for i in range(n)]
        else:
            if len(labels) != n:
                raise ValueError("Number of labels must correspond to length of ndarray A")
            self.labels = labels
        
        unsink = np.ones(n)
        for i in range(n):          # remove any and all sinks from the graph
            if sum(A[:,i]) == 0:
                self.Ahat[:,i] = unsink
            col_sum = sum(self.Ahat[:,i])
            self.Ahat[:,i] = self.Ahat[:,i] / col_sum

    # Problem 2
    def linsolve(self, epsilon=0.85):
        """Compute the PageRank vector using the linear system method.

        Parameters:
            epsilon (float): the damping factor, between 0 and 1.

        Returns:
            dict(str -> float): A dictionary mapping labels to PageRank values.
        """
        n = len(self.Ahat)
        I = np.eye(n)
        M = I - epsilon*self.Ahat
        b = (1-epsilon)/n * np.ones(n)
        p = np.linalg.solve(M,b)
        pageRankDic = {}            # create the page rank dictionary
        for i in range(n):
            pageRankDic[self.labels[i]] = p[i]
            
        return pageRankDic
            

    # Problem 2
    def eigensolve(self, epsilon=0.85):
        """Compute the PageRank vector using the eigenvalue method.
        Normalize the resulting eigenvector so its entries sum to 1.

        Parameters:
            epsilon (float): the damping factor, between 0 and 1.

        Return:
            dict(str -> float): A dictionary mapping labels to PageRank values.
        """
        n = len(self.Ahat)
        B = epsilon*self.Ahat + (1-epsilon)/n * np.ones((n,n))
        eig = np.linalg.eig(B)[1][:,0]
        p = eig / np.linalg.norm(eig,1)
        
        pageRankDic = {}            # create the page rank dictionary
        for i in range(n):
            pageRankDic[self.labels[i]] = p[i]
            
        return pageRankDic

    # Problem 2
    def itersolve(self, epsilon=0.85, maxiter=100, tol=1e-12):
        """Compute the PageRank vector using the iterative method.

        Parameters:
            epsilon (float): the damping factor, between 0 and 1.
            maxiter (int): the maximum number of iterations to compute.
            tol (float): the convergence tolerance.

        Return:
            dict(str -> float): A dictionary mapping labels to PageRank values.
        """
        n = len(self.Ahat)
        pt = np.ones(n)/n  # initial guess
        
        for i in range(maxiter):
            pt1 = epsilon*np.matmul(self.Ahat,pt) + (1-epsilon)/n * np.ones(n)           #p(t+1) function
            if np.linalg.norm(pt1-pt,1) < tol:
                pt = pt1
                break
            else:
                pt = pt1
            
        pageRankDic = {}            # create the page rank dictionary
        for i in range(n):
            pageRankDic[self.labels[i]] = pt[i]
            
        return pageRankDic

# Problem 3
def get_ranks(d):
    """Construct a sorted list of labels based on the PageRank vector.

    Parameters:
        d (dict(str -> float)): a dictionary mapping labels to PageRank values.

    Returns:
        (list) the keys of d, sorted by PageRank value from greatest to least.
    """
    keys = list(d.keys())
    values = list(d.values())
    sorted_keys = []
    
    while len(keys) > 0:
        cur_max = max(values)       # get the max value
        cur_index = values.index(cur_max)       # find the key that corresponds to the max value
        sorted_keys.append(keys[cur_index])     # append the current max key to our list
        del keys[cur_index]                     # remove the current key
        del values[cur_index]                   # remove the current value
    
    return sorted_keys


# Problem 4
def rank_websites(filename="web_stanford.txt", epsilon=0.85):
    """Read the specified file and construct a graph where node j points to
    node i if webpage j has a hyperlink to webpage i. Use the DiGraph class
    and its itersolve() method to compute the PageRank values of the webpages,
    then rank them with get_ranks(). If two webpages have the same rank,
    resolve ties by listing the webpage with the larger ID number first.

    Each line of the file has the format
        a/b/c/d/e/f...
    meaning the webpage with ID 'a' has hyperlinks to the webpages with IDs
    'b', 'c', 'd', and so on.

    Parameters:
        filename (str): the file to read from.
        epsilon (float): the damping factor, between 0 and 1.

    Returns:
        (list(str)): The ranked list of webpage IDs.
    """
    
    with open(filename) as file:
        all_ids = []
        for line in file:
            line_ids = line.split('/')
            all_ids += line_ids
        all_ids = list(map(int,all_ids))
        all_ids = list(OrderedDict.fromkeys(all_ids))   # remove duplicate IDs
        all_ids.sort()  # sort the unique id list by int size
        all_ids = list(map(str,all_ids))        # convert the list of ints into strings
        """Create dictionary with unique id key and index position as value"""
        n = len(all_ids)
        id_dict = {}
        for i in range(n):
            id_dict[all_ids[i]] = i
        
        """ Create the matrix of node j pointing to node i"""
        adjacency = np.zeros((n,n))
        file.seek(0)        # read through the file from the beginning again
        for line in file:
            line = line[:-1]    # remove the newline character from last index
            line_ids = line.split('/')
            start_col = id_dict[line_ids[0]]
            for ids in line_ids[1:]:
                end_col = id_dict[ids]
                adjacency[end_col,start_col] = 1
        graph = DiGraph(adjacency,all_ids)
        label_dict = graph.itersolve()
        results = get_ranks(label_dict)
        return results


# Problem 5
def rank_ncaa_teams(filename, epsilon=0.85):
    """Read the specified file and construct a graph where node j points to
    node i with weight w if team j was defeated by team i in w games. Use the
    DiGraph class and its itersolve() method to compute the PageRank values of
    the teams, then rank them with get_ranks().

    Each line of the file has the format
        A,B
    meaning team A defeated team B.

    Parameters:
        filename (str): the name of the data file to read.
        epsilon (float): the damping factor, between 0 and 1.

    Returns:
        (list(str)): The ranked list of team names.
    """
    with open(filename, 'r') as file:
        reader = csv.reader(file)
        next(reader)
        all_teams = []
        # get list of all team names
        for row in reader:
            all_teams += row
        all_teams = list(OrderedDict.fromkeys(all_teams))       # remove duplicate teams
        
        """ Create the dictionary with unique key representing the team and the value representing the index """
        n = len(all_teams)
        team_dict = {}
        for i in range(n):
            team_dict[all_teams[i]] = i
        
        """ Create the matrix of node j pointing to node i"""
        adjacency = np.zeros((n,n))
        
        """ Read through the file again and construct the adjacency matrix """
        file.seek(0)
        next(reader)
        for row in reader:
            winner = row[0]
            loser_index = team_dict[row[1]]
            winner_index = team_dict[row[0]]
            adjacency[winner_index,loser_index] += 1
            
        graph = DiGraph(adjacency,all_teams)
        label_dict = graph.itersolve()
        results = get_ranks(label_dict)
        return results


# Problem 6
def rank_actors(filename="top250movies.txt", epsilon=0.85):
    """Read the specified file and construct a graph where node a points to
    node b with weight w if actor a and actor b were in w movies together but
    actor b was listed first. Use NetworkX to compute the PageRank values of
    the actors, then rank them with get_ranks().

    Each line of the file has the format
        title/actor1/actor2/actor3/...
    meaning actor2 and actor3 should each have an edge pointing to actor1,
    and actor3 should have an edge pointing to actor2.
    """
    
    with open(filename, mode = 'r', encoding = 'utf-8') as file:
        
        # create directed graph
        DG = nx.DiGraph()
        
        for line in file:
            line = line[:-1]        # remove the newline character from each name
            line = line.split('/')
            actors = line[1:]       # remove the movie from the list of actors
            actor_pairs = itertools.combinations(actors,2)        # create tuples corresponding to (higher,lower) billing actor
            
            # create edges for the directed graph
            for pair in actor_pairs:
                if DG.has_edge(pair[1],pair[0]):        # if edge already exists, increment the weight
                    DG[pair[1]][pair[0]]["weight"] += 1
                else:
                    DG.add_edge(pair[1],pair[0], weight=1)            # new edge edge from lower to higher-billed actor
                    
        actorDict = nx.pagerank(DG, alpha=epsilon)
        actorRanks = get_ranks(actorDict)
        return actorRanks