# matplotlib_intro.py
"""Python Essentials: Intro to Matplotlib.
<Name> Brian James
<Class> Section 3
<Date> 9/14/2021
"""

import numpy as np
from matplotlib import pyplot as plt

# Problem 1
def var_of_means(n):
    """Construct a random matrix A with values drawn from the standard normal
    distribution. Calculate the mean value of each row, then calculate the
    variance of these means. Return the variance.

    Parameters:
        n (int): The number of rows and columns in the matrix A.

    Returns:
        (float) The variance of the means of each row.
    """
    
    A = np.random.normal(size=(n,n))
    row_means = np.mean(A,axis=1)
    return np.var(row_means)

def prob1():
    """Create an array of the results of var_of_means() with inputs
    n = 100, 200, ..., 1000. Plot and show the resulting array.
    """
    n_array = [(100*i) for i in range(1,11)]
    
    plt.plot(n_array,[var_of_means(element)for element in n_array])
    plt.show()
    return


# Problem 2
def prob2():
    """Plot the functions sin(x), cos(x), and arctan(x) on the domain
    [-2pi, 2pi]. Make sure the domain is refined enough to produce a figure
    with good resolution.
    """
    
    x = np.linspace(-2*np.pi,2*np.pi,500)
    y = np.sin(x)
    plt.plot(x,y)
    plt.show()
    y = np.cos(x)
    plt.plot(x,y)
    plt.show()
    y= np.arctan(x)
    plt.plot(x,y)
    plt.show()
    return


# Problem 3
def prob3():
    """Plot the curve f(x) = 1/(x-1) on the domain [-2,6].
        1. Split the domain so that the curve looks discontinuous.
        2. Plot both curves with a thick, dashed magenta line.
        3. Set the range of the x-axis to [-2,6] and the range of the
           y-axis to [-6,6].
    """
    x_1=np.linspace(-2,1,endpoint=False)
    x_2=np.linspace(1,6)[1:]
    y_1 = 1/(x_1-1)
    y_2 = 1/(x_2-1)
    plt.plot(x_1,y_1,'m--',linewidth=4)
    plt.plot(x_2,y_2,'m--',linewidth=4)
    plt.xlim(-2,6)
    plt.ylim(-6,6)
    plt.show()
    return


# Problem 4
def prob4():
    """Plot the functions sin(x), sin(2x), 2sin(x), and 2sin(2x) on the
    domain [0, 2pi].
        1. Arrange the plots in a square grid of four subplots.
        2. Set the limits of each subplot to [0, 2pi]x[-2, 2].
        3. Give each subplot an appropriate title.
        4. Give the overall figure a title.
        5. Use the following line colors and styles.
              sin(x): green solid line.
             sin(2x): red dashed line.
             2sin(x): blue dashed line.
            2sin(2x): magenta dotted line.
    """
    x=np.linspace(0,2*np.pi)
    
    ax1=plt.subplot(2,2,1)
    ax1.plot(x,np.sin(x),'g')
    plt.title("f(x)=sin(x)")
    plt.xlim(0,2*np.pi)
    plt.ylim(-2,2)
    
    
    ax2=plt.subplot(2,2,2)
    ax2.plot(x,np.sin(2*x),'r--')
    plt.title("f(x)=sin(2x)")
    plt.xlim(0,2*np.pi)
    plt.ylim(-2,2)
    
    ax3=plt.subplot(2,2,3)
    ax3.plot(x,2*np.sin(x),'b--')
    plt.title("f(x)=2sin(x)")
    plt.xlim(0,2*np.pi)
    plt.ylim(-2,2)
        
    ax4=plt.subplot(2,2,4)
    ax4.plot(x,2*np.sin(2*x),'m:')
    plt.title("f(x)=2sin(2x)")
    plt.xlim(0,2*np.pi)
    plt.ylim(-2,2)
    
    plt.show()
    return


# Problem 5
def prob5():
    """Visualize the data in FARS.npy. Use np.load() to load the data, then
    create a single figure with two subplots:
        1. A scatter plot of longitudes against latitudes. Because of the
            large number of data points, use black pixel markers (use "k,"
            as the third argument to plt.plot()). Label both axes.
        2. A histogram of the hours of the day, with one bin per hour.
            Label and set the limits of the x-axis.
    """
    data = np.load("FARS.npy")
    x=[row[1] for row in data]
    y = [row[2] for row in data]  
    ax1 = plt.subplot(1,2,1)
    ax1.axis("equal")
    ax1.set_xlabel("Longitude")
    ax1.set_ylabel("Latitude")
    ax1.plot(x,y,'ko')
    
    ax2 = plt.subplot(1,2,2)
    x=[row[0] for row in data]
    ax2.hist(x,bins=24, range=[0,24])
    ax2.set_xlabel("Hours of the day")
    plt.show()


# Problem 6
def prob6():
    """Plot the function f(x,y) = sin(x)sin(y)/xy on the domain
    [-2pi, 2pi]x[-2pi, 2pi].
        1. Create 2 subplots: one with a heat map of f, and one with a contour
            map of f. Choose an appropriate number of level curves, or specify
            the curves yourself.
        2. Set the limits of each subplot to [-2pi, 2pi]x[-2pi, 2pi].
        3. Choose a non-default color scheme.
        4. Add a colorbar to each subplot.
    """
    
    x = np.linspace(-2*np.pi,2*np.pi,500)
    y = np.linspace(-2*np.pi,2*np.pi,500)
    X, Y = np.meshgrid(x,y)
    Z = (np.sin(X) * np.sin(Y))/ (X * Y)    # create the function f
    
    plt.subplot(121)    # create the left subplot for the heatmap
    plt.pcolormesh(X,Y,Z,cmap="afmhot")
    plt.colorbar()
    plt.xlim(-2*np.pi,2*np.pi)
    plt.ylim(-2*np.pi,2*np.pi)
    
    plt.subplot(122)    # create the right subplot for the contour
    plt.contour(X,Y,Z,cmap="PiYG")
    plt.colorbar()
    plt.xlim(-2*np.pi,2*np.pi)
    plt.ylim(-2*np.pi,2*np.pi)
    
    plt.show()