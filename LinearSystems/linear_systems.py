# linear_systems.py
"""Volume 1: Linear Systems.
<Name> Brian James
<Class> Sec 03
<Date> 9/28/2021
"""

import numpy as np
from scipy import linalg as la
from scipy import sparse
from scipy.sparse.linalg import spsolve
import time
from matplotlib import pyplot as plt

# Problem 1
def ref(A):
    """Reduce the square matrix A to REF. You may assume that A is invertible
    and that a 0 will never appear on the main diagonal. Avoid operating on
    entries that you know will be 0 before and after a row operation.

    Parameters:
        A ((n,n) ndarray): The square invertible matrix to be reduced.

    Returns:
        ((n,n) ndarray): The REF of A.
    """
    A = np.array(A)
    n = len(A)
    
    for i in range(0,n-1):
        for j in range(i+1,n):
            c = A[j][i] / A[i][i]
            A[j][i+1:n+1] = A[j][i+1:n+1] - c * (A[i][i+1:n+1])
            A[j][i] = 0        
    return A


# Problem 2
def lu(A):
    """Compute the LU decomposition of the square matrix A. You may
    assume that the decomposition exists and requires no row swaps.

    Parameters:
        A ((n,n) ndarray): The matrix to decompose.

    Returns:
        L ((n,n) ndarray): The lower-triangular part of the decomposition.
        U ((n,n) ndarray): The upper-triangular part of the decomposition.
    """
    A = np.array(A)
    n= len(A)
    U = A.copy().astype(float)
    L = np.eye(n)
    
    for j in range(n-1):
        for i in range(j+1,n):
            L[i][j] = U[i][j]/U[j][j]
            U[i][j:] = U[i][j:] - L[i][j]*U[j][j:]
            
    return L,U


# Problem 3
def solve(A, b):
    """Use the LU decomposition and back substitution to solve the linear
    system Ax = b. You may again assume that no row swaps are required.

    Parameters:
        A ((n,n) ndarray)
        b ((n,) ndarray)

    Returns:
        x ((m,) ndarray): The solution to the linear system.
    """
    A = np.array(A)
    n = len(A[0])
    L,U = lu(A)
    y = np.vstack(np.zeros(n))     # Ly=b
    x = np.vstack(np.zeros(n))     #Ux=y

    print(L)
    print(U)

    y[0][0] = b[0][0]       # Initialize first element in y vector
    """ Find y vector """
    for k in range(1,n):
        y[k][0] = b[k][0] - sum([L[k][j]*y[j][0] for j in range(0,k)])
    
    x[n-1][0] = 1/U[n-1][n-1] * y[n-1][0]   # Initialize last element in x vector
    """ Find x vector """
    for k in range(n-2,-1,-1):
        x[k][0] = 1/U[k][k] * (y[k][0] - sum([U[k][j]*x[j][0] for j in range(k+1,n)]))

    return x


# Problem 4
def prob4():
    """Time different scipy.linalg functions for solving square linear systems.

    For various values of n, generate a random nxn matrix A and a random
    n-vector b using np.random.random(). Time how long it takes to solve the
    system Ax = b with each of the following approaches:

        1. Invert A with la.inv() and left-multiply the inverse to b.
        2. Use la.solve().
        3. Use la.lu_factor() and la.lu_solve() to solve the system with the
            LU decomposition.
        4. Use la.lu_factor() and la.lu_solve(), but only time la.lu_solve()
            (not the time it takes to do the factorization).

    Plot the system size n versus the execution times. Use log scales if
    needed.
    """
    n = [10,100,250,500,750,1000]
    end1, end2, end3, end4 = [], [], [], []
    
    for i in n:
        A = np.random.random((i,i))
        b = np.random.random(i)
        
        start1 = time.time()
        la.inv(A) * b
        end1.append(time.time() - start1)
        
        start2 = time.time()
        la.solve(A,b)
        end2.append(time.time() - start2)
        
        start3 = time.time()
        L, P = la.lu_factor(A)
        la.lu_solve((L,P),b)
        end3.append(time.time() - start3)
        
        start4 = time.time()
        la.lu_solve((L,P),b)
        end4.append(time.time() - start4)
    
    plt.semilogx(n,end1,'k',label="Inverse A * b")
    plt.semilogx(n,end2,'b',label="la.solve()")
    plt.semilogx(n,end3,'g',label="la.lu_factor() and la.lu_solve()")
    plt.semilogx(n,end4,'r',label="la.lu_solve()")
    plt.legend(loc="upper left")

    plt.show()

# Problem 5
def prob5(n):
    """Let I be the n × n identity matrix, and define
                    [B I        ]        [-4  1            ]
                    [I B I      ]        [ 1 -4  1         ]
                A = [  I . .    ]    B = [    1  .  .      ],
                    [      . . I]        [          .  .  1]
                    [        I B]        [             1 -4]
    where A is (n**2,n**2) and each block B is (n,n).
    Construct and returns A as a sparse matrix.

    Parameters:
        n (int): Dimensions of the sparse matrix B.

    Returns:
        A ((n**2,n**2) SciPy sparse matrix)
    """
    offsets = [-n,-1,0,1,n]
    
    A = sparse.diags([1,1,-4,1,1],offsets, shape=(n**2,n**2))
    M = A.toarray()
    
    return A

prob5(10)

# Problem 6
def prob6():
    """Time regular and sparse linear system solvers.

    For various values of n, generate the (n**2,n**2) matrix A described of
    prob5() and vector b of length n**2. Time how long it takes to solve the
    system Ax = b with each of the following approaches:

        1. Convert A to CSR format and use scipy.sparse.linalg.spsolve()
        2. Convert A to a NumPy array and use scipy.linalg.solve().

    In each experiment, only time how long it takes to solve the system (not
    how long it takes to convert A to the appropriate format). Plot the system
    size n**2 versus the execution times. As always, use log scales where
    appropriate and use a legend to label each line.
    """
    
    n = [10,25,50,75,100]
    end1, end2 = [], []
    for i in n:
        A = prob5(i)
        Acsr = A.tocsr()
        A_array = A.toarray()
        b = np.random.random(i**2)
        
        start = time.time()
        spsolve(Acsr,b)
        end1.append(time.time()-start)
        
        start = time.time()
        la.solve(A_array,b)
        end2.append(time.time()-start)
    
    plt.semilogx(n,end1,'k',label="CSR Format")
    plt.semilogx(n,end2,label="Numpy Array Format")
    plt.legend(loc="upper left")
    
    plt.show()
    
    
    
    