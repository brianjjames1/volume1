# qr_decomposition.py
"""Volume 1: The QR Decomposition.
<Name> Brian James
<Class> Sec 03
<Date> October 26, 2021
"""

import numpy as np
from scipy import linalg as la

# Problem 1
def qr_gram_schmidt(A):
    """Compute the reduced QR decomposition of A via Modified Gram-Schmidt.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n.

    Returns:
        Q ((m,n) ndarray): An orthonormal matrix.
        R ((n,n) ndarray): An upper triangular matrix.
    """
    n = len(A[0])
    
    Q = np.copy(A)
    R = np.zeros((n,n))
    
    for i in range(n):
        R[i,i]= la.norm(Q[:,i])
        Q[:,i] = Q[:,i] / R[i,i]
        for j in range(i+1,n):
            R[i,j] = np.dot(Q[:,j],Q[:,i])
            Q[:,j] = Q[:,j] - np.dot(R[i,j],Q[:,i])
    
    return Q,R


# Problem 2
def abs_det(A):
    """Use the QR decomposition to efficiently compute the absolute value of
    the determinant of A.

    Parameters:
        A ((n,n) ndarray): A square matrix.

    Returns:
        (float) the absolute value of the determinant of A.
    """
    
    return np.prod(np.diag(la.qr(A, mode="economic")[1]))


# Problem 3
def solve(A, b):
    """Use the QR decomposition to efficiently solve the system Ax = b.

    Parameters:
        A ((n,n) ndarray): An invertible matrix.
        b ((n, ) ndarray): A vector of length n.

    Returns:
        x ((n, ) ndarray): The solution to the system Ax = b.
    """
    n=len(A)
    Q,R = la.qr(A,mode="economic")
    y = np.dot(np.transpose(Q),b)
    x = np.zeros(n)
    """Back substitution loop"""
    for i in range(n-1,-1,-1):
        x[i] = (y[i] - sum([R[i,k]*x[k] for k in range(i+1,n)])) / R[i,i]
    
    return x


# Problem 4
def qr_householder(A):
    """Compute the full QR decomposition of A via Householder reflections.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n.

    Returns:
        Q ((m,m) ndarray): An orthonormal matrix.
        R ((m,n) ndarray): An upper triangular matrix.
    """
    m,n =np.shape(A)
    R = np.copy(A)
    Q = np.eye(m)
    
    for k in range(n):
        u = np.copy(R[k:,k])
        u[0] = u[0] + np.sign(u[0])*la.norm(u)
        if la.norm(u) != 0:
            u = u / la.norm(u)  # divide by zero error. Why is u zero? Can it ever be zero?
        R[k:,k:] = R[k:,k:] - 2*np.outer(u,(np.dot(np.transpose(u),R[k:,k:])))
        Q[k:,:] = Q[k:,:] - 2*np.outer(u,(np.dot(np.transpose(u),Q[k:,:])))
        
    return np.transpose(Q),R


# Problem 5
def hessenberg(A):
    """Compute the Hessenberg form H of A, along with the orthonormal matrix Q
    such that A = QHQ^T.

    Parameters:
        A ((n,n) ndarray): An invertible matrix.

    Returns:
        H ((n,n) ndarray): The upper Hessenberg form of A.
        Q ((n,n) ndarray): An orthonormal matrix.
    """
    m,n = np.shape(A)
    H = np.copy(A)
    Q = np.eye(m)
    for k in range(n-2):
        u = np.copy(H[k+1:,k])
        u[0] = u[0] + np.sign(u[0])*la.norm(u)
        if la.norm(u) != 0:
            u = u / la.norm(u)
        H[k+1:,k:] = H[k+1:,k:] - 2*np.outer(u,(np.dot(np.transpose(u),H[k+1:,k:])))
        H[:,k+1:] = H[:,k+1:] - 2 * np.outer(np.dot(H[:,k+1:],u),np.transpose(u))
        Q[k+1:,:] = Q[k+1:,:] - 2 * np.outer(u,np.dot(np.transpose(u),Q[k+1:,:]))
    
    return H, np.transpose(Q)

"""
A = [[-1,-11,-3],[1,1,0],[2,5,1]]
b = [-37,-1,10]
solve(A,b)
"""