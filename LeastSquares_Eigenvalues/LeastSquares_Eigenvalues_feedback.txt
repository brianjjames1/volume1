10/27/21 22:28

lstsq_eigs.py has not been modified yet

-------------------------------------------------------------------------------

10/30/21 10:44

lstsq_eigs.py has not been modified yet

-------------------------------------------------------------------------------

11/02/21 09:54

Problem 1 (5 points):
Score += 5

Problem 2 (5 points):
Score += 5

Problem 3 (10 points):
Polynomials appear to be one degree lower than intended (e.g. the degree-3 is actually quadratic)
Score += 9

Problem 4 (10 points):
Data points not plotted along with ellipse
Score += 5

Problem 5 (5 points):
Score += 5

Problem 6 (10 points):
qr_algorithm(A) failed for A =
[[2 6 5 3 8]
 [6 6 7 4 4]
 [5 7 4 3 4]
 [3 4 3 8 6]
 [8 4 4 6 4]]
	Correct response:
[-5.81 +0.j -2.055+0.j  1.662+0.j  5.325+0.j 24.878+0.j]
	Student response:
[-5.325+0.j -2.055+0.j  1.662+0.j  5.81 +0.j 24.878+0.j]
Score += 5

Code Quality (5 points):
Some functions, such as polynomial_fit(), could use more comments
Score += 3

Total score: 37/50 = 74.0%

-------------------------------------------------------------------------------

