# lstsq_eigs.py
"""Volume 1: Least Squares and Computing Eigenvalues.
<Name>
<Class>
<Date>
"""

# (Optional) Import functions from your QR Decomposition lab.
# import sys
# sys.path.insert(1, "../QR_Decomposition")
# from qr_decomposition import qr_gram_schmidt, qr_householder, hessenberg

import numpy as np
from matplotlib import pyplot as plt
from scipy import linalg
import cmath


# Problem 1
def least_squares(A, b):
    """Calculate the least squares solutions to Ax = b by using the QR
    decomposition.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n <= m.
        b ((m, ) ndarray): A vector of length m.

    Returns:
        x ((n, ) ndarray): The solution to the normal equations.
    """
    Q,R = linalg.qr(A,mode="economic")
    QT_B = np.dot(np.transpose(Q),b)
    return linalg.solve_triangular(R,QT_B)

# Problem 2
def line_fit():
    """Find the least squares line that relates the year to the housing price
    index for the data in housing.npy. Plot both the data points and the least
    squares line.
    """
    data = np.load("housing.npy")
    x = [[i[0],1] for i in data]
    y = [i[1] for i in data]
    A = np.vstack(x)
    b = np.vstack(y)
    ls_sol = least_squares(A,b)
    
    plt.scatter(data[0:,0],data[0:,1])
    plt.plot(data[0:,0],np.dot(A,ls_sol))
    
    plt.show()


# Problem 3
def polynomial_fit():
    """Find the least squares polynomials of degree 3, 6, 9, and 12 that relate
    the year to the housing price index for the data in housing.npy. Plot both
    the data points and the least squares polynomials in individual subplots.
    """
    data = np.load("housing.npy")
    degrees = [3,6,9,12]
    x = [i[0] for i in data]
    y= [i[1] for i in data]

    for j in degrees:
        A = np.vander(x,j)
        ls_sol = linalg.lstsq(A,y)[0]
        pos = int(j/3)
        plt.subplot(2,2,pos)
        plt.scatter(data[0:,0],data[0:,1])
        plt.plot(data[0:,0],np.dot(A,ls_sol))
    plt.show()


def plot_ellipse(a, b, c, d, e):
    """Plot an ellipse of the form ax^2 + bx + cxy + dy + ey^2 = 1."""
    theta = np.linspace(0, 2*np.pi, 200)
    cos_t, sin_t = np.cos(theta), np.sin(theta)
    A = a*(cos_t**2) + c*cos_t*sin_t + e*(sin_t**2)
    B = b*cos_t + d*sin_t
    r = (-B + np.sqrt(B**2 + 4*A)) / (2*A)

    plt.plot(r*cos_t, r*sin_t)
    plt.gca().set_aspect("equal", "datalim")

# Problem 4
def ellipse_fit():
    """Calculate the parameters for the ellipse that best fits the data in
    ellipse.npy. Plot the original data points and the ellipse together, using
    plot_ellipse() to plot the ellipse.
    """
    xk,yk = np.load("ellipse.npy").T
    A = np.column_stack((xk**2, yk**2, xk*yk, yk, yk**2))
    m = len(A)
    B = [[1] for i in range(m)]
    a,b,c,d,e = linalg.lstsq(A,B)[0]
    plot_ellipse(a,b,c,d,e)


# Problem 5
def power_method(A, N=20, tol=1e-12):
    """Compute the dominant eigenvalue of A and a corresponding eigenvector
    via the power method.

    Parameters:
        A ((n,n) ndarray): A square matrix.
        N (int): The maximum number of iterations.
        tol (float): The stopping tolerance.

    Returns:
        (float): The dominant eigenvalue of A.
        ((n,) ndarray): An eigenvector corresponding to the dominant
            eigenvalue of A.
    """
    m,n = np.shape(A)
    x_0 = np.random.random(n)   # random vector initialized
    if np.linalg.norm(x_0) != 0:
        x_0 = x_0 / np.linalg.norm(x_0) # normalize vector
    x_k = x_0
    for k in range(N):
        x_k1 = np.dot(A,x_k)
        if np.linalg.norm(x_k1) != 0:
            x_k1 = x_k1 / np.linalg.norm(x_k1)  # normalize vector
        if np.linalg.norm(x_k1 - x_k) < tol:    # if vector is within tolerance, break
            break
        x_k = x_k1
    return np.dot(np.transpose(x_k),np.dot(A,x_k)), x_k


# Problem 6
def qr_algorithm(A, N=50, tol=1e-12):
    """Compute the eigenvalues of A via the QR algorithm.

    Parameters:
        A ((n,n) ndarray): A square matrix.
        N (int): The number of iterations to run the QR algorithm.
        tol (float): The threshold value for determining if a diagonal S_i
            block is 1x1 or 2x2.

    Returns:
        ((n,) ndarray): The eigenvalues of A.
    """
    m,n = np.shape(A)
    S = linalg.hessenberg(A)
    for k in range(N):
        Q,R = linalg.qr(S)  # QR decomposition
        S = np.dot(R,Q)
    eigs = []   # initialize empty list of eigenvalues
    i = 0
    while i < n:
        if i == n-1:    # S_i is a 1x1 matrix since it is bottom-right element
            eigs.append(S[i,i])
        elif np.abs(S[i+1,i]) < tol: # S_i is a 1x1 matrix
            eigs.append(S[i,i])
        else:   # S_i is a 2x2 matrix
            lamb_1 = (-(S[i,i]+S[i+1,i+1]) + cmath.sqrt((S[i,i]+S[i+1,i+1])**2-4*(S[i,i]*S[i+1,i+1]-S[i,i+1]*S[i+1,i])))/2
            lamb_2 = (-(S[i,i]+S[i+1,i+1]) - cmath.sqrt((S[i,i]+S[i+1,i+1])**2-4*(S[i,i]*S[i+1,i+1]-S[i,i+1]*S[i+1,i])))/2
            eigs.append(lamb_1)
            eigs.append(lamb_2)
            i = i+1
        i= i+1
    return eigs   