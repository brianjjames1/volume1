# image_segmentation.py
"""Volume 1: Image Segmentation.
<Name>
<Class>
<Date>
"""

import numpy as np
import scipy
from scipy import linalg
from scipy.sparse.linalg import eigsh
from imageio import imread
from matplotlib import pyplot as plt

# Problem 1
def laplacian(A):
    """Compute the Laplacian matrix of the graph G that has adjacency matrix A.

    Parameters:
        A ((N,N) ndarray): The adjacency matrix of an undirected graph G.

    Returns:
        L ((N,N) ndarray): The Laplacian matrix of G.
    """
    m,n = np.shape(A)
    diagonal_list = []
    for row in A:
        sum = 0
        for j in range(n):
            sum = sum + row[j]
        diagonal_list.append(sum)
    diagonal_matrix = np.diag(diagonal_list)
    return diagonal_matrix - A


# Problem 2
def connectivity(A, tol=1e-8):
    """Compute the number of connected components in the graph G and its
    algebraic connectivity, given the adjacency matrix A of G.

    Parameters:
        A ((N,N) ndarray): The adjacency matrix of an undirected graph G.
        tol (float): Eigenvalues that are less than this tolerance are
            considered zero.

    Returns:
        (int): The number of connected components in G.
        (float): the algebraic connectivity of G.
    """
    L = laplacian(A)
    eigs = linalg.eig(L)    # return eigenvalue and eigenvector
    r_eigv = np.real(eigs[0])  # Extract the real eigenvalues from the complex eigenvalues
    num_connected = 0
    for i in r_eigv:
        if np.abs(i) < tol:
            num_connected = num_connected + 1
    r_eigv.sort()       # reorder the list of eigenvalues from smallest to largest
    second_last_eigv = r_eigv[1]     # get the second smallest eigenvalue, which represents the algebraic connectivity
    return num_connected, second_last_eigv


# Helper function for problem 4.
def get_neighbors(index, radius, height, width):
    """Calculate the flattened indices of the pixels that are within the given
    distance of a central pixel, and their distances from the central pixel.

    Parameters:
        index (int): The index of a central pixel in a flattened image array
            with original shape (radius, height).
        radius (float): Radius of the neighborhood around the central pixel.
        height (int): The height of the original image in pixels.
        width (int): The width of the original image in pixels.

    Returns:
        (1-D ndarray): the indices of the pixels that are within the specified
            radius of the central pixel, with respect to the flattened image.
        (1-D ndarray): the euclidean distances from the neighborhood pixels to
            the central pixel.
    """
    # Calculate the original 2-D coordinates of the central pixel.
    row, col = index // width, index % width

    # Get a grid of possible candidates that are close to the central pixel.
    r = int(radius)
    x = np.arange(max(col - r, 0), min(col + r + 1, width))
    y = np.arange(max(row - r, 0), min(row + r + 1, height))
    X, Y = np.meshgrid(x, y)

    # Determine which candidates are within the given radius of the pixel.
    R = np.sqrt(((X - col)**2 + (Y - row)**2))
    mask = R < radius
    return (X[mask] + Y[mask]*width).astype(np.int), R[mask]


# Problems 3-6
class ImageSegmenter:
    """Class for storing and segmenting images."""

    # Problem 3
    def __init__(self, filename):
        """Read the image file. Store its brightness values as a flat array."""
        image = imread(filename)
        self.scaled = image / 255
        if len(self.scaled.shape) == 3:   # check to see if the image has RBG attributes, AKA colored
            self.brightness = self.scaled.mean(axis=2)
            self.flat_brightness = np.ravel(self.brightness)
        else:   # if the image is
            self.flat_brightness = np.ravel(self.scaled)
        

    # Problem 3
    def show_original(self):
        """Display the original image."""
        if len(self.scaled.shape) == 2:
            plt.imshow(self.scaled, cmap="gray")
        else:
            plt.imshow(self.scaled)

    # Problem 4
    def adjacency(self, r=5., sigma_B2=.02, sigma_X2=3.):
        """Compute the Adjacency and Degree matrices for the image graph."""
        m,n = self.scaled.shape[0:2]    # extract the m and n from an image. Ignore the third shape if image is colored
        A = scipy.sparse.lil_matrix((m*n,m*n))
        D = np.zeros(m*n)
        
        neighbor_list = []
        distance_list = []
        for i in range(m*n):    #get all the neighbors and distances for each i in the 2D matrix
            neighbor,d = get_neighbors(i,r,m,n)
            neighbor_list.append(neighbor)
            distance_list.append(d)
        
        for i in range(m*n):    # calculate the weights for all ij pairs and insert into A one row at a time
            row = np.zeros(m*n)
            index = 0   # index to access the distance_list elements
            for j in neighbor_list[i]:
                # calculate the weight according to the formula
                w_ij = np.exp(-np.abs(self.flat_brightness[i]-self.flat_brightness[j])/sigma_B2 - distance_list[i][index]/sigma_X2)
                row[j] = w_ij
                index += 1
            A[i] = row
            D[i] = sum(row)
            
        A = A.tocsc()
        return A, D

    # Problem 5
    def cut(self, A, D):
        """Compute the boolean mask that segments the image."""
        L = scipy.sparse.csgraph.laplacian(A)
        D_neg1_2 = scipy.sparse.diags(D**(-1/2))
        dotted_matrix = (D_neg1_2.dot(L)).dot(D_neg1_2)
        eigvalues, eigvectors = eigsh(dotted_matrix,which="SM",k=2)
        second_sm_eigv = eigvectors[:,1]    # Get column vector that corresponds to second smallest eigenvalue
        m,n = self.scaled.shape[0:2]
        eigv_matrix = second_sm_eigv.reshape(m,n)
        mask = eigv_matrix > 0  # segment based on positive and nonpositive values
        return mask

    # Problem 6
    def segment(self, r=5., sigma_B=.02, sigma_X=3.):
        """Display the original image and its segments."""
        plt.subplot(1,3,1)
        self.show_original()

        A,D = self.adjacency(r,sigma_B,sigma_X)
        mask = self.cut(A,D)
        
        if len(self.scaled.shape) == 3: # plot the colored images
            if self.scaled.shape[2] == 4:    # this means the image has alpha channels
                third_dim_mask = np.dstack((mask,mask,mask,mask))
            else:   # this colored image has RGB channels
                third_dim_mask = np.dstack((mask,mask,mask))
            plt.subplot(1,3,2)
            plt.imshow(third_dim_mask*self.scaled)
            
            plt.subplot(1,3,3)
            plt.imshow(~third_dim_mask*self.scaled)
        
        else:                           # plot the grayscale images
            plt.subplot(1,3,2)
            plt.imshow(mask*self.scaled)
            
            plt.subplot(1,3,3)
            plt.imshow(~mask*self.scaled)
        
        plt.show()


# if __name__ == '__main__':
#     ImageSegmenter("dream_gray.png").segment()
#     ImageSegmenter("dream.png").segment()
#     ImageSegmenter("monument_gray.png").segment()
#     ImageSegmenter("monument.png").segment()