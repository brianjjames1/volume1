# newtons_method.py
"""Volume 1: Newton's Method.
<Name> Brian James
<Class>
<Date>
"""

import numpy as np
from scipy import optimize
from matplotlib import pyplot as plt
from numpy import linalg as la

# Problems 1, 3, and 5
def newton(f, x0, Df, tol=1e-5, maxiter=15, alpha=1.):
    """Use Newton's method to approximate a zero of the function f.

    Parameters:
        f (function): a function from R^n to R^n (assume n=1 until Problem 5).
        x0 (float or ndarray): The initial guess for the zero of f.
        Df (function): The derivative of f, a function from R^n to R^(nxn).
        tol (float): Convergence tolerance. The function should returns when
            the difference between successive approximations is less than tol.
        maxiter (int): The maximum number of iterations to compute.
        alpha (float): Backtracking scalar (Problem 3).

    Returns:
        (float or ndarray): The approximation for a zero of f.
        (bool): Whether or not Newton's method converged.
        (int): The number of iterations computed.
    """
    convergence_status = False
    if np.isscalar(x0):                         # if x0 is a scalar
        F = lambda x : x - alpha * f(x)/Df(x)
    else:                                       # if x0 is multi-dimensional
        F = lambda x : x - alpha * la.solve(Df(x),f(x))
    
    for k in range(maxiter):        # iterate through newton's method up to maxiter times
        x1 = F(x0)
        if la.norm(x1-x0) < tol:        # if estimate is accurate enough, break
            convergence_status = True
            x0 = x1
            break
        x0 = x1
    
    return x0, convergence_status, k


# Problem 2
def prob2(N1, N2, P1, P2):
    """Use Newton's method to solve for the constant r that satisfies

                P1[(1+r)**N1 - 1] = P2[1 - (1+r)**(-N2)].

    Use r_0 = 0.1 for the initial guess.

    Parameters:
        P1 (float): Amount of money deposited into account at the beginning of
            years 1, 2, ..., N1.
        P2 (float): Amount of money withdrawn at the beginning of years N1+1,
            N1+2, ..., N1+N2.
        N1 (int): Number of years money is deposited.
        N2 (int): Number of years money is withdrawn.

    Returns:
        (float): the value of r that satisfies the equation.
    """
    r_0 = 0.1   # Initial guess
    
    f = lambda r : P1 * ((1+r)**N1 -1) - P2 * (1-(1+r)**(-N2))      # function f
    Df = lambda r : P1*N1*(1+r)**(N1-1) + P2*N2*(1+r)**(-N2-1)      # derivative of f
    return newton(f,r_0,Df)[0]                                      # return the root r
# Problem 4
def optimal_alpha(f, x0, Df, tol=1e-5, maxiter=15):
    """Run Newton's method for various values of alpha in (0,1].
    Plot the alpha value against the number of iterations until convergence.

    Parameters:
        f (function): a function from R^n to R^n (assume n=1 until Problem 5).
        x0 (float or ndarray): The initial guess for the zero of f.
        Df (function): The derivative of f, a function from R^n to R^(nxn).
        tol (float): Convergence tolerance. The function should returns when
            the difference between successive approximations is less than tol.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        (float): a value for alpha that results in the lowest number of
            iterations.
    """
    
    alpha = np.linspace(0,1,50)                                     # create range of possible alpha values
    alpha = np.delete(alpha,0)                                      # remove 0 from the alpha values
    iterList = []
    for a in alpha:                                                 # find how many iterations it takes for each alpha value to reach a good estimate within tolerance value
        iterList.append(newton(f,x0,Df,tol,maxiter,a)[2])
    
    plt.plot(alpha,iterList)
    plt.show()


# Problem 6
def prob6():
    """Consider the following Bioremediation system.

                              5xy − x(1 + y) = 0
                        −xy + (1 − y)(1 + y) = 0

    Find an initial point such that Newton’s method converges to either
    (0,1) or (0,−1) with alpha = 1, and to (3.75, .25) with alpha = 0.55.
    Return the intial point as a 1-D NumPy array with 2 entries.
    """
    
    f = lambda x : np.array([5*x[0]*x[1] - x[0]*(1+x[1]),-x[0]*x[1] + (1-x[1])*(1+x[1])])
    Df = lambda x : np.array([[5*x[1]-(1+x[1]),4*x[0]],[-x[1],-x[0]-2*x[1]]])
    
    x_v = np.linspace(-1/4,0,75)
    y_v = np.linspace(0,1/4,75)
    
    # Construct a combination of (x,y) pairs
    xy_0 = []               # xy_0 will be a matrix 
    for x in x_v:
        row = []
        for y in y_v:
            ele = (x,y)
            row.append(ele)
        if len(xy_0) == 0:
            xy_0 = np.array(row)
        else:
            xy_0 = np.vstack((xy_0,np.array(row)))
    
    # Find when newton's method converges for (0,1) or (0,-1) with alpha=1
    for initial in xy_0:
        try:
            x= newton(f,initial,Df,1e-5,15,1)
            if np.allclose(x[0],[0,1]) or np.isclose(x[0],[0,-1]):
                # Now check to see if initial converges to (3.75,.25) if alpha =.55
                x= newton(f,initial,Df,1e-5,70,.55)
                if np.allclose(x[0],[3.75,.25]):            
                    return initial
        except Exception:
            pass


# Problem 7
def plot_basins(f, Df, zeros, domain, res=1000, iters=15):
    """Plot the basins of attraction of f on the complex plane.

    Parameters:
        f (function): A function from C to C.
        Df (function): The derivative of f, a function from C to C.
        zeros (ndarray): A 1-D array of the zeros of f.
        domain ([r_min, r_max, i_min, i_max]): A list of scalars that define
            the window limits and grid domain for the plot.
        res (int): A scalar that determines the resolution of the plot.
            The visualized grid has shape (res, res).
        iters (int): The exact number of times to iterate Newton's method.
    """
    
    x_real = np.linspace(domain[0],domain[1],res)               # create a space of real numbers
    x_imag = np.linspace(domain[2],domain[3],res)               # create a space of imaginary numbers
    X_real, X_imag = np.meshgrid(x_real,x_imag)
    X_0 = X_real + 1j*X_imag                                    # create a matrix of complex numbers

    F = lambda x : x - f(x)/Df(x)                               # define function for newton method with alpha = 1.
    
    for k in range(iters):                                      # iterate through newton's method iters times
        X_1 = F(X_0)
        X_0 = X_1
    n = len(X_0)
    Y = np.zeros((n,n))                                         # initialize nxn matrix
    for i in range(n):
        for j in range(n):
            cur_array = X_0[i,j]-zeros      # Calculate the difference between element of X matrix and the zeros
            Y[i,j] = np.argmin(np.abs(cur_array))       # find index that is closest to zero of f
    
    plt.pcolormesh(x_real,x_imag,Y,cmap='brg')
    plt.show()