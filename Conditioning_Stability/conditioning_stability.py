# condition_stability.py
"""Volume 1: Conditioning and Stability.
<Name> Brian James
<Class> Vol 1
<Date>
"""

import numpy as np
import sympy as sy
from scipy import linalg
from matplotlib import pyplot as plt


# Problem 1
def matrix_cond(A):
    """Calculate the condition number of A with respect to the 2-norm."""
    singular_vals = linalg.svdvals(A)
    max_s = singular_vals[0]
    min_s = singular_vals[-1:]
    if min_s == 0:
        return np.inf
    else:
        return max_s/min_s


# Problem 2
def prob2():
    """Randomly perturb the coefficients of the Wilkinson polynomial by
    replacing each coefficient c_i with c_i*r_i, where r_i is drawn from a
    normal distribution centered at 1 with standard deviation 1e-10.
    Plot the roots of 100 such experiments in a single figure, along with the
    roots of the unperturbed polynomial w(x).

    Returns:
        (float) The average absolute condition number.
        (float) The average relative condition number.
    """
    w_roots = np.arange(1, 21)

    # Get the exact Wilkinson polynomial coefficients using SymPy.
    x, i = sy.symbols('x i')
    w = sy.poly_from_expr(sy.product(x-i, (i, 1, 20)))[0]
    w_coeffs = np.array(w.all_coeffs())
    abs_list = []
    rel_list = []
    
    for j in range(100):        # repeat this 100 times
        perturb_coeffs = w_coeffs
        h = []
        for i in range(len(perturb_coeffs)):
            r_i = np.random.normal(1,1e-10)
            perturb_coeffs[i] = w_coeffs[i] * r_i
            h.append(1-r_i)
        new_roots = np.roots(np.poly1d(perturb_coeffs))
        plt.plot(new_roots.real,new_roots.imag,',')
        
        abs_k = linalg.norm(new_roots-w_roots,np.inf) / linalg.norm(h,np.inf)
        rel_k = abs_k * linalg.norm(w_coeffs,np.inf) / linalg.norm(w_roots,np.inf)
        abs_list.append(abs_k)
        rel_list.append(rel_k)
        
    plt.plot(w_roots.real,w_roots.imag,'ob')        # plot the unperturbed roots
    plt.show()
    
    return np.mean(abs_list), np.mean(rel_list)


# Helper function
def reorder_eigvals(orig_eigvals, pert_eigvals):
    """Reorder the perturbed eigenvalues to be as close to the original eigenvalues as possible.
    
    Parameters:
        orig_eigvals ((n,) ndarray) - The eigenvalues of the unperturbed matrix A
        pert_eigvals ((n,) ndarray) - The eigenvalues of the perturbed matrix A+H
        
    Returns:
        ((n,) ndarray) - the reordered eigenvalues of the perturbed matrix
    """
    n = len(pert_eigvals)
    sort_order = np.zeros(n).astype(int)
    dists = np.abs(orig_eigvals - pert_eigvals.reshape(-1,1))
    for _ in range(n):
        index = np.unravel_index(np.argmin(dists), dists.shape)
        sort_order[index[0]] = index[1]
        dists[index[0],:] = np.inf
        dists[:,index[1]] = np.inf
    return pert_eigvals[sort_order]

# Problem 3
def eig_cond(A):
    """Approximate the condition numbers of the eigenvalue problem at A.

    Parameters:
        A ((n,n) ndarray): A square matrix.

    Returns:
        (float) The absolute condition number of the eigenvalue problem at A.
        (float) The relative condition number of the eigenvalue problem at A.
    """
    # Create perturbation H (complex number)
    reals = np.random.normal(0, 1e-10, A.shape)
    imags = np.random.normal(0, 1e-10, A.shape)
    H = reals + 1j*imags
    
    A_eigval = linalg.eigvals(A)            # A eigenvalues
    Ahat_eigval = linalg.eigvals(A+H)     # A+H eigenvalues
    Ahat_eigval = reorder_eigvals(A_eigval,Ahat_eigval)
    
    abs_k = linalg.norm(A_eigval-Ahat_eigval,2) / linalg.norm(H,2)
    rel_k = abs_k * linalg.norm(A,2) / linalg.norm(A_eigval,2)
    return abs_k, rel_k


# Problem 4
def prob4(domain=[-100, 100, -100, 100], res=50):
    """Create a grid [x_min, x_max] x [y_min, y_max] with the given resolution. For each
    entry (x,y) in the grid, find the relative condition number of the
    eigenvalue problem, using the matrix   [[1, x], [y, 1]]  as the input.
    Use plt.pcolormesh() to plot the condition number over the entire grid.

    Parameters:
        domain ([x_min, x_max, y_min, y_max]):
        res (int): number of points along each edge of the grid.
    """
    x = np.linspace(domain[0],domain[1],res)
    y = np.linspace(domain[2],domain[3],res)
    
    
    # generate cartesian product
    xgrid, ygrid = np.meshgrid(x,y)
    rel_k_list = np.zeros((res,res))
    
    for i in range(len(xgrid)):
        for j in range(len(xgrid[0:])):
            A = np.array([[1,xgrid[i,j]],[ygrid[i,j],1]])
            rel_k_list[i,j] = eig_cond(A)[1]
            
            
    #Plot the condition number of the entire grid
    plt.pcolormesh(xgrid,ygrid,rel_k_list,cmap='gray_r')
    plt.colorbar(label="relative condition of eigenvalues")
    plt.xlabel("x values")
    plt.ylabel("y values")
    plt.show()


# Problem 5
def prob5(n):
    """Approximate the data from "stability_data.npy" on the interval [0,1]
    with a least squares polynomial of degree n. Solve the least squares
    problem using the normal equation and the QR decomposition, then compare
    the two solutions by plotting them together with the data. Return
    the mean squared error of both solutions, ||Ax-b||_2.

    Parameters:
        n (int): The degree of the polynomial to be used in the approximation.

    Returns:
        (float): The forward error using the normal equations.
        (float): The forward error using the QR decomposition.
    """
    
    xk, yk = np.load("stability_data.npy").T
    A = np.vander(xk, n+1)
    AT = A.transpose()
    
    #Method 1
    x1 = np.dot(linalg.inv(np.dot(AT,A)),np.dot(AT,yk))
    error_1 = linalg.norm(np.dot(A,x1)-yk,2)
    
    #Method 2
    Q,R = linalg.qr(A,mode='economic')
    QT = Q.transpose()
    x2 = linalg.solve_triangular(R,np.dot(QT,yk))
    error_2 = linalg.norm(np.dot(A,x2)-yk,2)
    
    #Plot method 1
    plt.subplot(121)
    plt.plot(xk,yk,'ob')
    plt.plot(xk,np.polyval(x1,xk),'orange')
    plt.xlabel("x values")
    plt.ylabel("y values")
    plt.title("Normal Equation")
    
    #Plot method 2
    plt.subplot(122)
    plt.plot(xk,yk,'ob')
    plt.plot(xk,np.polyval(x2,xk),'purple')
    plt.xlabel("x values")
    plt.ylabel("y values")
    plt.title("QR Decomposition")
    
    plt.show()
    
    return error_1, error_2


# Problem 6
def prob6():
    """For n = 5, 10, ..., 50, compute the integral I(n) using SymPy (the
    true values) and the subfactorial formula (may or may not be correct).
    Plot the relative forward error of the subfactorial formula for each
    value of n. Use a log scale for the y-axis.
    """
    x = sy.Symbol('x')
    
    n_list = 5*np.arange(1,11)
    data_true = []
    data_subfac = []
    rel_error = []
    
    for n in n_list:
        #Compute the true integral of the function
        f = x**n * np.e**(x-1)
        I_true = sy.integrate(f,(x,0,1))
        data_true.append(I_true)
        
        #Compute the subfactorial method of integration
        I_subfac = (-1)**n*(sy.subfactorial(n)-sy.factorial(n)/np.e)
        data_subfac.append(I_subfac)
        
        #Compute relative error
        rel_error.append(np.abs(I_true-I_subfac)/np.abs(I_true))
        
    plt.plot(n_list,rel_error)
    plt.yscale("log")
    plt.ylabel("relative error")
    plt.xlabel("n values")
    plt.show()