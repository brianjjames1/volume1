11/19/21 12:39

Problem 1 (5 points):
Please run the cells of the notebook
Score += 0

Problem 2 (5 points):
0
Score += 0

Problem 3 (5 points):
Please run the cells of the notebook
Score += 0

Problem 4 (10 points):
Please run the cells of the notebook
Score += 0

Problem 5 (10 points):
Score += 10

Problem 6 (10 points):
Please run the cells of the notebook
Score += 0

Code Quality (5 points):
More comments would be good
Score += 2

Total score: 12/50 = 24.0%

-------------------------------------------------------------------------------

11/20/21 13:22

Problem 1 (5 points):
Run the cells of the notebook before pushing
Score += 0

Problem 2 (10 points):
Run the cells of the notebook before pushing
Score += 0

Problem 3 (10 points):
Run the cells of the notebook before pushing
Score += 0

Problem 4 (10 points):
Run the cells of the notebook before pushing
Score += 0

Problem 5 (5 points):
Score += 5

Problem 6 (5 points):
Run the cells of the notebook before pushing
Score += 0

Code Quality (5 points):
The functions in FacialRec could use more comments
Score += 2

Total score: 7/50 = 14.0%

-------------------------------------------------------------------------------

11/23/21 17:24

Problem 1 (5 points):
Run the cell that displays the image before pushing
Score += 2

Problem 2 (10 points):
Run the cell that displays the images before pushing
Score += 5

Problem 3 (10 points):
Run the cell that displays the image before pushing
Score += 5

Problem 4 (10 points):
Run the cell that displays the images before pushing
Score += 0

Problem 5 (5 points):
Score += 5

Problem 6 (5 points):
Run the cell that displays the images before pushing
Score += 0

Code Quality (5 points):
Some functions, such as FacialRec.find_nearest(), could use more comments
Score += 2

Total score: 19/50 = 38.0%

-------------------------------------------------------------------------------

12/07/21 18:54

Problem 1 (5 points):
Score += 5

Problem 2 (10 points):
Score += 10

Problem 3 (10 points):
Only one eigenface displayed
Score += 8

Problem 4 (10 points):
Reconstructions not labeled
Score += 8

Problem 5 (5 points):
Score += 5

Problem 6 (5 points):
Score += 5

Code Quality (5 points):
Score += 5

Total score: 46/50 = 92.0%

Great job!

-------------------------------------------------------------------------------

