09/10/21 17:46

standard_library.py has not been modified yet

-------------------------------------------------------------------------------

09/15/21 12:43

Problem 1 (5 points):
Score += 5

Problem 2 (5 points):
NotImplementedError: Problem 2 Incomplete

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
	power_set(A) does not contain set(A)
	power_set(A) does not contain set(A)
	power_set(A) does not contain set(A)
	power_set(A) does not contain set(A)
Score += 0

Problem 5 (15 points):
Some output lines are printed on the same line as each other
Score += 14

Code Quality (5 points):
Score += 5

Total score: 34/50 = 68.0%

-------------------------------------------------------------------------------

10/07/21 19:55

Problem 1 (5 points):
Score += 5

Problem 2 (5 points):
NotImplementedError: Problem 2 Incomplete

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
	power_set(A) does not contain set(A)
	power_set(A) does not contain set(A)
	power_set(A) does not contain set(A)
	power_set(A) does not contain set(A)
Score += 0

Problem 5 (15 points):
Score += 15

Code Quality (5 points):
Score += 5

Total score: 35/50 = 70.0%


Comments:
	When you start working on a problem, remove the "raise NotImplementedError(...)" at the bottom of the problem; not doing so looks messy and will confuse the grading system in some cases

-------------------------------------------------------------------------------

