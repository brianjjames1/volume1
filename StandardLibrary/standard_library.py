# standard_library.py
"""Python Essentials: The Standard Library.
<Name> Brian James
<Class> Section 3
<Date> 9/14/2021
"""

import calculator
import itertools
import box
import random
import sys
import time


# Problem 1
def prob1(L):
    """Return the minimum, maximum, and average of the entries of L
    (in that order).
    """
    return min(L), max(L), sum(L)/len(L)
    
    raise NotImplementedError("Problem 1 Incomplete")


# Problem 2
def prob2():
    """Determine which Python objects are mutable and which are immutable.
    Test numbers, strings, lists, tuples, and sets. Print your results.
    """
    
    print("My results are as follows:")
    print("numbers: immutable")
    print("strings: immutable")
    print("lists: mutable")
    print("tuples: immutable")
    print("sets: mutable")
    
    raise NotImplementedError("Problem 2 Incomplete")


# Problem 3
def hypot(a, b):
    """Calculate and return the length of the hypotenuse of a right triangle.
    Do not use any functions other than sum(), product() and sqrt that are 
    imported from your 'calculator' module.

    Parameters:
        a: the length one of the sides of the triangle.
        b: the length the other non-hypotenuse side of the triangle.
    Returns:
        The length of the triangle's hypotenuse.
    """
    
    return calculator.sqrt(calculator.sum(calculator.product(a, a),calculator.product(b,b)))
    
    raise NotImplementedError("Problem 3 Incomplete")


# Problem 4
def power_set(A):
    """Use itertools to compute the power set of A.

    Parameters:
        A (iterable): a str, list, set, tuple, or other iterable collection.

    Returns:
        (list(sets)): The power set of A as a list of sets.
    """
    
    powerSet = [set()]
    listSize = len(A)
    i=0
    j=[]
    
    for i in range (1,listSize+1):
        for j in list(itertools.combinations(A,i)):
            powerSet.append(j)
        
    return powerSet
    
    raise NotImplementedError("Problem 4 Incomplete")
    


def dice_roll(number_list):
    """ Randomly simulate rolling the dice. If the total of the remaining numbers is less than 6,
    roll only one die. Otherwise, roll two dice.
    
    Parameters:
        number_list: the list of numbers currently in play.
    """
    
    if sum(number_list) <= 6:
        roll = random.randint(1,6)
    else:
        roll = random.randint(1,12)
    return roll

def time_left(time_limit, start_time):
    """ Return the amount of seconds left for the game
    
    Parameters:
        time_limit: the amount of time the player chose to start with.
        start_time: the beginning time.
    """
    
    current_time = time.time()
    time_elapsed = current_time - start_time
    return time_limit - time_elapsed

# Problem 5: Implement shut the box.
def shut_the_box(player, timelimit):
    """Play a single game of shut the box."""
    first_prompt = True
    remaining_numbers = [1,2,3,4,5,6,7,8,9]
    timelimit = float(timelimit)
    start_time = 0      # initialize
    win_condition = False
    
    while True:
        
        if not first_prompt and time_left(timelimit, start_time) <= 0:
            print("Game over!")
            break
    
        print("\nNumbers left: " + str(remaining_numbers))
        roll = dice_roll(remaining_numbers)
        print("Roll: " + str(roll))
        
        # Calculate if numbers can match roll. Otherwise, break!
        if not box.isvalid(roll, remaining_numbers):
            print("Game over!")
            break
        
        
        while True:
            if first_prompt:
                print("Seconds left: " + "{:.2f}".format(timelimit), end='')
                first_prompt = False
                start_time = time.time()        #first occasion time is measured
            else:
                print("Seconds left: " + "{:.2f}".format(time_left(timelimit,start_time)), end='')
                
            
            num_selection = input("Numbers to eliminate: ")
            
            parsed_input = box.parse_input(num_selection, remaining_numbers)
            
            if len(parsed_input) > 0 and sum(parsed_input) == roll:     # exit loop if user entered valid integers.
                #print("\n")
                remaining_numbers = [x for x in remaining_numbers if x not in parsed_input]
                break;
            else:
                print("Invalid input\n")
            if time_left(timelimit, start_time) <= 0:
                break
                
        if len(remaining_numbers) == 0:     # Win condition
            win_condition = True
            break

    # Player report
    finishing_time = time.time()
    print("Score for player " + player + ": " + str(sum(remaining_numbers)))
    print("Time played: " + "{:.2f}".format(finishing_time-start_time) + " seconds")
    if win_condition:
        print("Congratulations! You shut the box!")
    else:
        print("Better luck next time >:)")
    return
    




if __name__ == "__main__":
    if len(sys.argv) == 3:
        shut_the_box(sys.argv[1],sys.argv[2])
    else:
        print("Incorrect arguments for shut the box")