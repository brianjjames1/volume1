#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 13 21:58:17 2021

@author: James
"""

from math import sqrt

def sum(a,b):
    """Return the sum of the numbers a and b.
    
    Parameters
    a: the number left of the plus sign.
    b: the number right of the plus sign.
    
    Returns: the sum of a and b
    
    """
    return a + b

def product(a,b):
    """Return the product of the numbers a and b.
    
    Parameters
    a: the number left of the multiplication sign.
    b: the number right of the multiplication sign.
    
    Returns: the product of a and b
    
    """
    return a * b

    