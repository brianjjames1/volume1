# montecarlo_integration.py
"""Volume 1: Monte Carlo Integration.
<Name> Brian James
<Class> Volume 1
<Date>
"""

import numpy as np
from scipy import linalg as la
from scipy import stats
from matplotlib import pyplot as plt

# Problem 1
def ball_volume(n, N=10000):
    """Estimate the volume of the n-dimensional unit ball.

    Parameters:
        n (int): The dimension of the ball. n=2 corresponds to the unit circle,
            n=3 corresponds to the unit sphere, and so on.
        N (int): The number of random points to sample.

    Returns:
        (float): An estimate for the volume of the n-dimensional unit ball.
    """
    
    points = np.random.uniform(-1,1,(n,N))          #Sample N points over our domain
    
    lengths = la.norm(points,axis=0)
    num_within = np.count_nonzero(lengths < 1)
    
    return 2**n * (num_within/N)


# Problem 2
def mc_integrate1d(f, a, b, N=10000):
    """Approximate the integral of f on the interval [a,b].

    Parameters:
        f (function): the function to integrate. Accepts and returns scalars.
        a (float): the lower bound of interval of integration.
        b (float): the lower bound of interval of integration.
        N (int): The number of random points to sample.

    Returns:
        (float): An approximation of the integral of f over [a,b].

    Example:
        >>> f = lambda x: x**2
        >>> mc_integrate1d(f, -4, 2)    # Integrate from -4 to 2.
        23.734810301138324              # The true value is 24.
    """
    
    V = b - a
    points = np.random.uniform(a,b,N)
    f_val = f(points)
    return (V / N) * sum(f_val)     # return the approximation of the integral

# Problem 3
def mc_integrate(f, mins, maxs, N=10000):
    """Approximate the integral of f over the box defined by mins and maxs.

    Parameters:
        f (function): The function to integrate. Accepts and returns
            1-D NumPy arrays of length n.
        mins (list): the lower bounds of integration.
        maxs (list): the upper bounds of integration.
        N (int): The number of random points to sample.

    Returns:
        (float): An approximation of the integral of f over the domain.

    Example:
        # Define f(x,y) = 3x - 4y + y^2. Inputs are grouped into an array.
        >>> f = lambda x: 3*x[0] - 4*x[1] + x[1]**2

        # Integrate over the box [1,3]x[-2,1].
        >>> mc_integrate(f, [1, -2], [3, 1])
        53.562651072181225              # The true value is 54.
    """
    b_a = [maxs[i]-mins[i] for i in range(len(mins))]       # b-a list
    V = np.prod(b_a)
    points = np.random.uniform(0,1,(len(mins),N))
    adjusted_points = points * np.array(b_a)[:,None] + np.array(mins)[:,None]    #b_a * points + mins
    f_val = []    
    for col in adjusted_points.transpose():     # evaluate the function at each component
        f_val.append(f(col))
    return (V / N) * sum(f_val)


# Problem 4
def prob4():
    """Let n=4 and Omega = [-3/2,3/4]x[0,1]x[0,1/2]x[0,1].
    - Define the joint distribution f of n standard normal random variables.
    - Use SciPy to integrate f over Omega.
    - Get 20 integer values of N that are roughly logarithmically spaced from
        10**1 to 10**5. For each value of N, use mc_integrate() to compute
        estimates of the integral of f over Omega with N samples. Compute the
        relative error of estimate.
    - Plot the relative error against the sample size N on a log-log scale.
        Also plot the line 1 / sqrt(N) for comparison.
    """
    
    f = lambda x : np.exp(-np.matmul(x.transpose(),x)/2) / ((2*np.pi)**(n/2))       # the function to integrate
    n=4
    mins = np.array([-3/2,0,0,0])
    maxs = np.array([3/4,1,1/2,1])
    mc_i = mc_integrate(f,mins,maxs)
    
    means, cov = np.zeros(4), np.eye(4)
    F = stats.mvn.mvnun(mins, maxs, means, cov)[0]      # Compute the "exact" integral
    
    logspace = np.logspace(1,5,num=20).astype(int)      # Get roughly logarithmically spaced points
    rel_error = []
    for N in logspace:
        approx_F = mc_integrate(f,mins,maxs,N)
        rel_error.append(np.abs(F-approx_F) / np.abs(F))
        
    
    # Plot the relative error vs the logspace and the 1/√N line
    plt.loglog(logspace,rel_error,label='Relative Error',marker='o')
    plt.loglog(logspace,1/np.sqrt(logspace),label='1/√N',marker='o')
    plt.legend()
    plt.show()

prob4()